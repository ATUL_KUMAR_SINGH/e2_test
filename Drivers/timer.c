#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_misc.h"
#include "timer.h"
#include "packet.h"
#include "uart.h"
#include "port_mapping.h"
#include "registration.h"
#include "system.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "led.h"
#include "water_management.h"
#include "buzzer.h"
#include "schedule.h"




uint32_t PrescalerValue = 0, CCR1_Val = 0;
__IO uint64_t timer_value = 0; 
uint8_t display_led_pattern_flag;
uint16_t sensor_sec_count, sensor_monitoring, pump_monitoring, adc_monitoring, feedback_monitoring;


uint8_t volatile buzzer_behaviour_kill_indication_time;


extern volatile uint8_t	beep_once_flag;
extern volatile uint8_t	beep_2sec;
volatile uint8_t beep_2sec_counter;
//uint16_t buzzer_activation_counter;
//uint8_t buzzer_activation_flag;   //atul

extern volatile uint8_t wifi_module_count,wifi_module_flag_set, start_low_atcmd_pin;

volatile uint32_t last_system_notification_info;
extern uint32_t system_notification_info;

/*
**===========================================================================
**		variable declaration
**===========================================================================
*/
//uint16_t sensor_sec_count, udp_sec_count_glbl;	// for counting 1 second timer 5
//extern uint8_t chk_dry_run_oht_flag, chk_dry_run_ugt_flag,buzzer_indication_num, prev_buzzer_indication_num, buzzer_behavior[8];
////__IO uint64_t timer2_value = 0, timer3_value = 0, timer4_value = 0, timer5_value = 0, timer9_value = 0, timer10_value = 0, timer11_value = 0, timer12_value = 0, timer13_value = 0, timer14_value = 0, micro_sec_cnt = 0, minutes_cnt = 0,\
//// milli_sec_cnt = 0, timer_value = 0, tub_cnt = 0, temp_chk_counter = 0, data = 0, clk_cntr = 0, uart_pkt_wait_cnt; 
//extern uint16_t day_time_in_minutes_glbl,dry_run_oht_counter, dry_run_ugt_counter;
//volatile uint32_t driver_time_out;
//extern volatile uint8_t wdgReloadByTimer, ugt_pump_trigger_flag, ugt_pump_trigger_count,  oht_pump_trigger_flag, oht_pump_trigger_count;
//extern uint8_t oht_key_debounce_flag, oht_key_debounce_counter, oht_switch_status_curr, oht_switch_status_prev, ugt_key_debounce_flag, ugt_key_debounce_counter, ugt_switch_status_curr, ugt_switch_status_prev, oamp_key_debounce_flag, oamp_key_debounce_counter, oamp_switch_status_curr, oamp_switch_status_prev, ip_get_flag_glbl;
//uint8_t oht_switch_low_flag, oht_switch_low_count, ugt_switch_low_flag, ugt_switch_low_count, oamp_switch_low_flag, oamp_switch_low_count, one_second_flag, one_min_flag, ip_chk_count_glbl, set_schedule_flag;
////uint32_t  CCR1_Val = 0;
//extern tank_schedule oht_schd, ugt_schd, buzzer_schd;
////extern uint32_t udp_broadcast_counter;
//extern uint16_t pump_evt_num;
//extern uint8_t packet_process_count, packet_process_flag, adc_flag, module_count;

//uint8_t gui_packet_process_flag;

//extern RTC_DateTypeDef date_obj, RTC_DateStruct;
//extern RTC_TimeTypeDef RTC_TimeStructure;

//volatile uint8_t StopMode_Measure_flag;
//volatile uint8_t StopMode_Measure_count;


//extern struct auto_task auto_task_info;

//#ifdef MULTI_TANK_ENABLE
//	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
//	extern struct tank_status status;	
//#else
//	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
//#endif

//extern struct buzzer_switch_behavior buzzer_switch_info;
//extern struct buzzer_interval 	buzzer_interval_info;

// volatile uint8_t notification_counter_oht;

//uint8_t presence_sensor_return = 1;

//volatile uint8_t wifi_socket_250_ms_count, wifi_socket_250_ms_flag;

volatile uint8_t Indication_bootloader_cnt, Indication_bootloader_flag;
//volatile uint8_t Indication_packet_received, Indication_packet_processed;
 volatile uint16_t timeout_app, repeated_request_timeout ;


/* External Variables ------------------------------------------------------------*/
/* Qos */
/* oamp */
/* ntw settings */

TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;

void TIM1_CC_IRQHandler (void) {
	if (TIM_GetITStatus(TIM1, TIM_IT_CC1) != RESET) {
		TIM_ClearITPendingBit (TIM1, TIM_IT_CC1);
	}  
}




void TIM16_IRQHandler (void) {  	
	static uint8_t display_time_count;
	static uint32_t ugt_switch_count_ms, oht_switch_count_ms, oamp_switch_count_ms;
	uint8_t  case_sel = 0;

	if (TIM_GetITStatus (TIM16, TIM_IT_Update) != RESET) {
		
		
		

		Indication_bootloader_cnt++;
		if(Indication_bootloader_cnt++ >= 40){				//Bootloader indication 400 ms
				Indication_bootloader_cnt = 0;
				Indication_bootloader_flag = 1;	
		}
			
		
		
		repeated_request_timeout++;   //100 * 10 = 1000 milisecond

	}

		
	/***********************************************/
	TIM_ClearITPendingBit (TIM16, TIM_IT_Update);
	/***********************************************/

	
}
	
	
	







//void TIM14_IRQHandler(void) {

//	if (TIM_GetITStatus(TIM14, TIM_IT_Update) != RESET)	{

//		timeout_app++;
//		module_count--;

//		
//		
//		TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
//  	}

//}
// 		
                            



void TIM17_IRQHandler(void) {
	
//uint8_t buzzer_activation_flag;
static uint16_t toggle_count, toggle_flag;
	
	if(TIM_GetITStatus(TIM17, TIM_IT_Update) != RESET){
		
//		if(buzzer_activation_flag == 1){
//			 if(toggle_count++ >= 8){			//14	// 2.56 khz  ////15*8 = 120+120 = 240 //4.1 khz
//					GPIO_ToggleBits(GPIOB, GPIO_Pin_4);
//				  toggle_count = 0;
//			}
//		}
		
//	toggle_count++;                    //testing code
//	if(toggle_count == 8)
//	{
//		GPIO_ToggleBits(GPIOA, GPIO_Pin_3);
//		toggle_count = 0;
//	}

	TIM_ClearITPendingBit(TIM17, TIM_IT_Update);
	}

}


///************************************************************************//**
//				void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type)
//*
//* @brief		This function enables the Timer3 Interrupt.
//*
//* @param		TIMx = Timer no. can be	TIM3, TIM4, TIM5
//*				timer_type can be TIME_MICRO_SEC, TIME_MILLI_SEC, TIME_SEC, TIME_MINUTE, TIME_HOUR.
//*
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Sahil Saini
//* @date			23/05/13
//*
//* @note			
//****************************************************************************/
//void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type){
//	if(timer_type == TIME_MICRO_SEC) {
//		CCR1_Val = 24;
//	} else if(timer_type == TIME_MILLI_SEC)	{
//		CCR1_Val = 2399;
//       // CCR1_Val = 4799;
//	} else if(timer_type == TIME_SEC) {
//		CCR1_Val = 47999;
//	} else if(timer_type == TIME_MINUTE) {
//		CCR1_Val = 1439940;
//	} else if(timer_type == TIME_HOUR) {
//		CCR1_Val = 86396400;
//	}
//	
//	TIM_DeInit(TIMx);
//	/* Time base configuration */										
//	TIM_TimeBaseStructure.TIM_Period = CCR1_Val ;				   		// Autoreload register value initilization
//	TIM_TimeBaseStructure.TIM_Prescaler = timer_value - 1;	
//	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1;    //TIM_CounterMode_Up;
//	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
//	
//	/* Output Compare Timing Mode configuration: Channel1 */
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_Pulse = CCR1_Val ;		 //<**************
//	TIM_OC1Init(TIMx, &TIM_OCInitStructure);
//	
//	TIM_OC1PreloadConfig(TIMx, TIM_OCPreload_Disable);
//	TIM_ARRPreloadConfig(TIMx, ENABLE); 
//
//	if (TIMx == TIM6)
//		TIM_ITConfig(TIMx, TIM_IT_Update , ENABLE);
//	else
//		/* TIM Interrupts enable */
//		TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
//		
//	/* TIM5 enable counter */
////	TIM_Cmd(TIMx, ENABLE);
//}
//
///************************************************************************//**
//				void TIM_Interrupt_Config(void)
//*
//* @brief		This function enables the Timer Interrupt.
//*
//* @param		None
//*
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Aman Deep & Uday
//* @date			22/05/12
//*
//* @note			
//****************************************************************************/
//void timer_nvic_config(TIM_TypeDef* TIMx){
//	NVIC_InitTypeDef NVIC_InitStructure;
//
//	if(TIMx == TIM1){
//		/* TIM2 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
//		
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	if(TIMx == TIM2){
//		/* TIM2 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//		
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	else if(TIMx == TIM3){
//		/* TIM3 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
//
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	else if(TIMx == TIM6){
//		/* TIM3 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
//
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM6_DAC_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/	
//	else if(TIMx == TIM14){
//		/* TIM5 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM15){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM15_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM16){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM17){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM17_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
//	
//	/* TIMx Interrupts enable */
//	TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
//}
//
///************************************************************************//**
//				uint64_t configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd)
//*
//* @brief		This function Configures the Timer Interrupt Value.
//*
//* @param		[1]timer_value_recvd : Resemble the time to be set accordingly to the time flag receieved.
//*				[2]time_flag_rcvd : Time recieved in required format.
//
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Sahil Saini
//* @date			22/05/13
//*
//* @note			
//****************************************************************************/
//void configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd){
//
//	switch(time_flag_rcvd){
//
//		case TIME_MICRO_SEC:
//			timer_value = (uint64_t)(timer_value_recvd);  						// Time in Micro Sec
//			break;
//		case TIME_MILLI_SEC:
//			timer_value = ((uint64_t)(timer_value_recvd)*10); 					// Time in Milli Sec
//			break;
//		case TIME_SEC:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000); 				// Time in Sec
//			break;
//		case TIME_MINUTE:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000);			  		// Time in Minutes
//			break;
//		case TIME_HOUR:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000);	 				//Time in Hours
//			break;
//		default :
//		 	timer_value = 0;  	
//	}  
//}
///************************************************************************//**
//				void timer_init(uint8_t timer_no, uint8_t timer_type, uint64_t timer_val)
//*
//* @brief		This function Configures the Timer Interrupt Value.
//*
//* @param		[1]timer_no : initialized timer number
//*	
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Aman Deep
//* @date			08/08/12
//*
//* @note			
//****************************************************************************/
//void timer_init(TIM_TypeDef* TIMx, uint8_t timer_type, uint16_t timer_val) {
//	configure_timer_value (timer_val, timer_type);
//	if (TIMx == TIM6)
//		timer_value *= 2;
//	timer_nvic_config(TIMx);
//	TIMER_OC_Config(TIMx, timer_type);
//}










/************************************************************************//**
    void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type)
*
* @brief  This function enables the Timer3 Interrupt.
*
* @param  TIMx = Timer no. can be TIM3, TIM4, TIM5
*    timer_type can be TIME_MICRO_SEC, TIME_MILLI_SEC, TIME_SEC, TIME_MINUTE, TIME_HOUR.
*
* @returns  None
*
* @exception None.
*
* @author  Sahil Saini
* @date   23/05/13
*
* @note   
****************************************************************************/
void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type, uint32_t timer_val){
 TIM_TypeDef timer_vl;
 uint32_t timer_calc;
 float temp;
 temp  = SystemCoreClock/1000000;
 temp = 1/temp;
 timer_calc = timer_val/temp;
   
 if(timer_type == TIME_MICRO_SEC) {
  temp  = SystemCoreClock/1000000;
  CCR1_Val = (SystemCoreClock/(temp*1000000));
 } 
 else if(timer_type == TIME_MILLI_SEC) {
  temp  = SystemCoreClock/1000000;
  CCR1_Val = (SystemCoreClock/(temp*1000));
  } 
 else if(timer_type == TIME_SEC) {
   temp  = SystemCoreClock/1000000;
   CCR1_Val = (SystemCoreClock/(temp*1));
  }


 TIM_DeInit(TIMx);
 /* Time base configuration */          
 TIM_TimeBaseStructure.TIM_Period =  timer_calc - 1;     //207;    // Autoreload register value initilization
 TIM_TimeBaseStructure.TIM_Prescaler = CCR1_Val - 1;    // 0; 
 TIM_TimeBaseStructure.TIM_ClockDivision = 0;
 TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;   //TIM_CounterMode_CenterAligned1;
// TIM_TimeBaseStructure.TIM_RepetitionCounter = 1;
 TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
 
 /* TIM Interrupts enable */
  TIM_ITConfig(TIMx, TIM_IT_Update , ENABLE);
}
/************************************************************************//**
    void TIM_Interrupt_Config(void)
*
* @brief  This function enables the Timer Interrupt.
*
* @param  None
*
* @returns  None
*
* @exception None.
*
* @author  Aman Deep & Uday
* @date   22/05/12
*
* @note   
****************************************************************************/
void timer_nvic_config(TIM_TypeDef* TIMx){
 NVIC_InitTypeDef NVIC_InitStructure;
 if(TIMx == TIM1){
  /* TIM2 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
  
  /* Enable the TIM3 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/
// if(TIMx == TIM2){
//  /* TIM2 clock enable */
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//  
//  /* Enable the TIM3 gloabal Interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority =0;
// }
 /**********************************************************/
 else if(TIMx == TIM3){
  /* TIM3 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
  /* Enable the TIM3 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
 }
 /**********************************************************/
 else if(TIMx == TIM14){
  /* TIM5 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
  
  /* Enable the TIM12 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/   
// else if(TIMx == TIM15){
//  /* TIM5 clock enable */
//  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
//  
//  /* Enable the TIM12 gloabal Interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = TIM15_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
// }
 /**********************************************************/   
 else if(TIMx == TIM16){
  /* TIM5 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);
  
  /* Enable the TIM16 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/   
 else if(TIMx == TIM17){
  /* TIM5 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
  
  /* Enable the TIM12 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM17_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
 }
 /**********************************************************/  
 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 NVIC_Init(&NVIC_InitStructure);
 
 /* TIMx Interrupts enable */
// TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
}
/************************************************************************//**
    uint64_t configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd)
*
* @brief  This function Configures the Timer Interrupt Value.
*
* @param  [1]timer_value_recvd : Resemble the time to be set accordingly to the time flag receieved.
*    [2]time_flag_rcvd : Time recieved in required format.
* @returns  None
*
* @exception None.
*
* @author  Sahil Saini
* @date   22/05/13
*
* @note   
****************************************************************************/
void configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd){
 switch(time_flag_rcvd){
  case TIME_MICRO_SEC:
   timer_value = (uint64_t)(timer_value_recvd);        // Time in Micro Sec
   break;
  case TIME_MILLI_SEC:
   timer_value = ((uint64_t)(timer_value_recvd)*10);      // Time in Milli Sec
   break;
  case TIME_SEC:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);     // Time in Sec
   break;
  case TIME_MINUTE:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);       // Time in Minutes
   break;
  case TIME_HOUR:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);      //Time in Hours
   break;
  default :
    timer_value = 0;   
 }  
}
/************************************************************************//**
    void timer_init(uint8_t timer_no, uint8_t timer_type, uint64_t timer_val)
*
* @brief  This function Configures the Timer Interrupt Value.
*
* @param  [1]timer_no : initialized timer number
* 
* @returns  None
*
* @exception None.
*
* @author  Aman Deep
* @date   08/08/12
*
* @note   
****************************************************************************/
void timer_init(TIM_TypeDef* TIMx, uint8_t timer_type, uint16_t timer_val) {
 timer_nvic_config(TIMx);
 TIMER_OC_Config(TIMx, timer_type, timer_val);
}

#include "stm32f0xx_adc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "adc.h"

#include "std_periph_headers.h"


uint64_t converted_data_recvd_adc1 = 0, adc1_counter = 0;
volatile uint64_t averaged_adc1_value;

uint16_t milli_volts_at_feedback,battery_voltage_millivolt;
float feedback_cnvrtd_voltage ;

volatile uint8_t end_of_conversion = 0;

uint8_t adc_counter;   

uint16_t readADC1(uint32_t channel) {
//	ADC_StopOfConversion (ADC1);
	/* Clear the selected ADC Channel */
	ADC1->CHSELR = 0;
	/* Convert the ADC1 input with 55.5 Cycles as sampling time */
	ADC_ChannelConfig(ADC1, channel, ADC_SampleTime_55_5Cycles); // PA.1 - IN1	// Start the conversion
	/* ADC1 regular Software Start Conv */
	ADC_StartOfConversion(ADC1);	// Wait until conversion completion
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	// Get the conversion value
	return (((ADC_GetConversionValue(ADC1)) * 3300)/0x03FF);
}


void adc_init (void) {
	ADC_InitTypeDef ADC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* GPIOC Periph clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Configure ADC Channel12, Channel13, Channel14, Channel15 as analog input */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* PCLK is the APB2 clock */
	/* ADCCLK = PCLK/4 = 48/4 = 12MHz*/
//	RCC_ADCCLKConfig(RCC_ADCCLK_PCLK_Div4);

	/* ADC1 Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/* ADC1 Periph clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/* Put everything back to power-on defaults */
	ADC_DeInit(ADC1);

	/* ADC1 Configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;    
//	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T3_TRGO;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
	ADC_Init(ADC1, &ADC_InitStructure); 

	ADC_ChannelConfig(ADC1, ADC_Channel_0 , ADC_SampleTime_239_5Cycles); 

	/* ADC Calibration */
//	ADC_GetCalibrationFactor(ADC1);

	ADC_interrupt_config();

    ADC_ITConfig (ADC1, ADC_IT_EOC, ENABLE);
	
//	/* Enable the auto delay feature */    
//	ADC_WaitModeCmd(ADC1, ENABLE); 
	
//	/* Enable the Auto power off mode */
//	ADC_AutoPowerOffCmd(ADC1, ENABLE); 
	
	/* Enable ADCperipheral[PerIdx] */
	ADC_Cmd(ADC1, ENABLE);     
	
	/* Wait the ADCEN falg */
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_ADEN)); 
	
//	/* TIM2 enable counter */
//	TIM_Cmd(TIM3, ENABLE);
//	
//	/* ADC1 regular Software Start Conv */ 
	ADC_StartOfConversion(ADC1);

}


void ADC_interrupt_config () {

  NVIC_InitTypeDef  NVIC_InitStructure;
 
  NVIC_InitStructure.NVIC_IRQChannel = ADC1_COMP_IRQn;
 /// NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
 // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
}

void ADC1_COMP_IRQHandler(void)
{  

	if (ADC_GetITStatus (ADC1,ADC_IT_EOC) == SET) {
		ADC_ClearITPendingBit (ADC1,ADC_IT_EOC);	
		
	//	if(adc_flag){
	//	adc_flag=0;

		converted_data_recvd_adc1 += ADC_GetConversionValue (ADC1);		
		adc1_counter++;
		
		if (adc1_counter >= 99) {

			/* Disabling ADC */
		    ADC_ITConfig (ADC1, ADC_IT_EOC, DISABLE);
		    ADC_Cmd (ADC1, DISABLE);
			averaged_adc1_value = converted_data_recvd_adc1/100;

		    battery_voltage_millivolt = averaged_adc1_value * 3300 / 0xFFF; 
			//feedback_cnvrtd_voltage = averaged_adc1_value * 3300 / 0xFFF;        //test changes krishan	 27 jan	  				
		    //battery_voltage_millivolt = feedback_cnvrtd_voltage;			     //test changes krishan	 27 jan
								
			converted_data_recvd_adc1 = 0;			
			adc1_counter = 0;			
			
			end_of_conversion = 1;
			adc_counter=0;                                                  // reset the adc counter           
		 }
	 // }				
	}
}


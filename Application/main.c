/************************************************************************//**
* @file			main.c
*
* @brief		This file contains functions required for Boot Loader.
*               
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/11/2011 \n by \em Seema Sharma
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			Separate functions can be used for accessing word or dword 
* 				as access to dword is fast as compared to a word.            			
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/

#include "stm32f0xx.h"
#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "i2c.h"
#include "eeprom.h"
#include "led.h"
#include "pin_config.h"
#include "watchdog.h"
#include "stm32f0xx_rcc.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "profile.h"
#include "rtc.h"
#include "water_management.h"
#include "buzzer.h"
#include "memory_map.h"
#include "json_client.h"
#include "client_registration.h"
#include "buyers_n_client_reg.h"
#include "stm32f0xx_syscfg.h"
#include "schedule.h"
#include "string.h"



extern volatile uint8_t Indication_bootloader_cnt, Indication_bootloader_flag;



/************************************************************************//**
*				void main(void)
*
* @brief		Initialize clock, keypad, uart and timer
*               Reads Sensirion Temperature Sensor(SHT15) every 30 Secs
* 				Enable Global Interrupt and other Low Power Mode Interrupts.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			Module Usage details and other notes.
****************************************************************************/
void main(void){

	Init_Target ();
//	GPIO_WriteBit(GPIOA, GPIO_Pin_5,Bit_SET);
GPIO_SetBits(GPIOA, GPIO_Pin_5);
	while(1){

//		if(Indication_bootloader_flag) {   //Bootloader running indication
//			Indication_bootloader_flag = 0;
//			GPIO_ToggleBits(GPIOA, GPIO_Pin_4);
//		}
  
	}//while(1)


}//main



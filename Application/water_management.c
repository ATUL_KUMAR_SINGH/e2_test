/********************************************************************************************************************************
 * File name:	 	water_manaegment.c
 *
 * Attention:		Copyright 2013 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 30 april 2014 by Nikhil Kukreja
 *
 * Description: 	This module is used to initialize & control Water Level Management System.
 * 					This module controls motors, alarms, buzzer, leds, performs water level measurement and
 * 					consumption data logging.
 *******************************************************************************************************************************/

/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "json_client.h"
#include "water_management.h"
#include "stdio.h"
#include "sensor.h"
#include "buyers_n_client_reg.h"
//#include "AT45DB161D.h"
#include "eeprom.h"
//#include "ethernet_packet.h"
#include "rtc.h"				   
#include "automated_tasks.h"
#include "client_registration.h"
#include "buzzer.h"
#include "schedule.h"
#include "string.h"
//#include "logs.h"
//#include "tcp.h"
#include "memory_map.h"
#include <string.h>
#include <stdio.h>
//#include <RTL.h>
//#include <Net_Config.h>




#define	NOT_DEFINE						2
#define OFF   0


/*
**===========================================================================
**		structures declaration
**===========================================================================
*/
struct sys_info system_info;
struct wms_sys_info wms_sys_config;
//struct backup_info wms_bckup_config;
struct real_sensor_info  oht_real_sensor_config[TOTAL_OHT_TANK], ugt_real_sensor_config;
struct virtual_sensor_info oht_virtual_sensor_config, ugt_virtual_sensor_config;
struct tank_setting oht_tank_config[TOTAL_OHT_TANK], ugt_tank_config;
struct pump_info  oht_pump_config, ugt_pump_config;
extern struct auto_task auto_task_info;
extern struct buzzer_switch_behavior buzzer_switch_info;
extern struct alarm_time oht_tank_schedule, ugt_tank_schedule;
extern uint32_t oht_time_bw_sensor[10],ugt_time_bw_sensor[8];
extern uint32_t oht_inflow_rate, ugt_inflow_rate, oht_total_filled_time, ugt_total_filled_time;
//extern LOCALM localm[];
//extern LOCALM ip_config;
//extern struct client_info	http_user_info[TOTAL_HTTP_SESSION];
extern struct auto_task_state auto_task_pump_state[9];
const uint8_t Neotech_Header[] __attribute__((section(".ARM.__at_0x08000400"))) = "AMF201110004011306020001NEOTECH PVT LTD                                                                                                    ";


#ifdef MULTI_TANK_ENABLE
	struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	struct tank_status status;	
#else
	struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif							    


struct tank_immediate_setting oht_immed, ugt_immed;
extern RTC_TimeTypeDef last_pump_triggered_time_OHT, last_pump_triggered_time_UGT;
extern RTC_DateTypeDef last_pump_triggered_date_OHT, last_pump_triggered_date_UGT;
extern uint8_t OHT_pump_state, UGT_pump_state, OHT_triggered, UGT_triggered;

/*
**===========================================================================
**		variables declaration
**===========================================================================
*/
extern uint8_t total_ugt_motor_log, total_oht_motor_log, current_oht_log_num, current_ugt_log_num;
extern uint32_t current_day_oht_consumption, current_day_ugt_consumption;
extern uint8_t oht_flow_rate_present, ugt_flow_rate_present, oht_tank_calibrated, ugt_tank_calibrated;
uint8_t buzzer_set_level;
uint8_t otal_oht_consumption_logs, total_ugt_consumption_logs, oht_virtual_enable_gui_flag, ugt_virtual_enable_gui_flag, license_class, total_client_license;
extern uint32_t oht_inflow_rate, ugt_inflow_rate, oht_total_filled_time;
//const uint8_t fw_ver[] = {3,7,28};
/* The following values are read out of EEPROM. */
uint8_t mac_adr[6], def_mac_addr[6] = {0xa4, 0x4f, 0x29, 0xe0, 0x04, 0x01},dev_name[16] = { "aquasmart" }; //= { 0x1E,0x30,0x6C,0xA2,0x89,0x56 }, dev_name[16] = { "wms123" };

/*
**===========================================================================
**		global structures description
**===========================================================================
*	struct tank_info 	- 	contains information about tank settings, current volume, current day consumtion,
*					   		sensor informations.
*	struct wms_sys_info	-	contains information about total tanks, total pumps,
*							port numbers of tanks.
*	struct backup_info	-   holds the tank dimesnsions.
*
*	struct real_sensor_info	- holds the total sensors, current sensor status, 
*							  highest set sensor, malfunctioning sensor status.
*
*	struct virtual_sensor_info - holds the virtual total sensors, current sensor status, 
*							  	highest set sensor, malfunctioning sensor status.	
*
*	struct tank_setting		- contains tank information like tank number, tank type, 
*							  total volume and other parameters.
*
*	struct pump_info		- contins pump state, discharge capacity, last trigger state
*							  last on/off time, trigger cause.
*
*/


/*===========================================================================
**
**		Function Prototype Section
**===========================================================================*/

/********************************************************************************************************************************
 * Function name: 	void restore_configured_parameters(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	30 april,2014
 *
 * Date modified:	30 april,2014
 *
 * Description: 	This function is used to initialize all WMS parameters.
 *
 * Notes:
 *******************************************************************************************************************************/
void initialize_global_structures(void){
	uint8_t count;

	
	for(count = 0; count < TOTAL_OHT_TANK; count++){
		OHT_tank[count].real_sensor_ptr 	= 	&oht_real_sensor_config[count];
		OHT_tank[count].tank_config_ptr = 	&oht_tank_config[count];
	}
	
	UGT_tank.real_sensor_ptr = 	&ugt_real_sensor_config;
	UGT_tank.virtual_sensor_ptr = 	&ugt_virtual_sensor_config;
	UGT_tank.tank_config_ptr = 	&ugt_tank_config;	   


	
	status.oht_pump_ptr = 	(struct pump_info *)&oht_pump_config;
	status.ugt_pump_ptr = 	(struct pump_info *)&ugt_pump_config;

	//buzzer_switch_info.buzzer_activate_flag = 1;
}

//void write_one_time_settings(void){
//	uint8_t temp_arr[50], loc = 0;
//	
//	/* Write board status */
//	temp_arr[0] = 0xa4;temp_arr[1] = 0x4f;temp_arr[2] = 0x29;temp_arr[3] = 0xe0;temp_arr[4] = 0x04,temp_arr[5] = 0x05;//	loc	= 'N';
////	dflash_write_multiple_byte (SYSTEM_INFO_ADDR, &loc, 1);		   	/* board status */
//
//	/* write system mac id */
//	dflash_write_multiple_byte (SYSTEM_MAC_ID_INFO_ADDR, &temp_arr[0], 6);	/* system mac id */
//	loc	= 1;    /* dhcp enable */
//	dflash_write_multiple_byte(DHCP_INFO_ADDR, &loc, 1);
//
//	/* write license number */
//	temp_arr[0] = '1';temp_arr[1] = '2';temp_arr[2] = '3';temp_arr[3] = '4';temp_arr[4] = '5',temp_arr[5] = '6', temp_arr[6] = '7';temp_arr[7] = '8';temp_arr[8] = '9';temp_arr[9] = 'A';
//	temp_arr[10] = '1';temp_arr[11] = '2';temp_arr[12] = '3';temp_arr[13] = '4';temp_arr[14] = '5',temp_arr[15] = '6', temp_arr[16] = '7';temp_arr[17] = '8';temp_arr[18] = '9';temp_arr[19] = 'B';
//	temp_arr[20] = '1';temp_arr[21] = '2';temp_arr[22] = '3';temp_arr[23] = '6';
//	dflash_write_multiple_byte(LICENSE_NUM_INFO_ADDR, &temp_arr[0], 24);	   /* write license num */
//	
//	
//	/* write serial number */
//	temp_arr[0] = 'A';temp_arr[1] = 'S';temp_arr[2] = '0';temp_arr[3] = '1';temp_arr[4] = '0',temp_arr[5] = '1', temp_arr[6] = '0';temp_arr[7] = '0';temp_arr[8] = '0';temp_arr[9] = '0';
//	temp_arr[10] = '0';temp_arr[11] = '0';temp_arr[12] = '1';temp_arr[13] = '0';
//	dflash_write_multiple_byte(SERIAL_NUM_INFO_ADDR, &temp_arr[0], SERIAL_NUM_LEN);	   /* write license num */	
//	/* */
//	temp_arr[0] = 3;temp_arr[1] = 7;temp_arr[2] = 10;							
//	dflash_write_multiple_byte(CURRENT_FW_ADDR, &temp_arr[0], VERSION_NUM_LEN);	   /* write license num */
//	temp_arr[0] = 0;temp_arr[1] = 0;temp_arr[2] = 0;							
//	dflash_write_multiple_byte(UPGRADING_FW_ADDR, &temp_arr[0], VERSION_NUM_LEN);	   /* write license num */	
//}

void write_one_time_settings(void){
	uint8_t temp_arr[50], loc = 0;
	
//	dflash_change_pagesize();
	/* Write board status */
	
	

    temp_arr[0] = 0xA4;
	  temp_arr[1] = 0x4F;
	  temp_arr[2] = 0x29;
	  temp_arr[3] = 0xE0;
	  temp_arr[4] = 0x04,
	  temp_arr[5] = 0x5A;
	
	 //	loc	= 'N';
//	temp_arr[0] = 0xa4;temp_arr[1] = 0x4f;temp_arr[2] = 0x29;temp_arr[3] = 0xe0;temp_arr[4] = 0x01,temp_arr[5] = 0x23;//	loc	= 'N';
//	dflash_write_multiple_byte (SYSTEM_INFO_ADDR, &loc, 1);		   	/* board status */

	/* write system mac id */
	eeprom_data_read_write (SYSTEM_MAC_ID_INFO_ADDR, WRITE_OP, &temp_arr[0], 6);	/* system mac id */
	loc	= 1;    /* dhcp enable */
	eeprom_data_read_write(DHCP_INFO_ADDR, WRITE_OP, &loc, 1);
//	PWE5513406503B54F8EOFBRL
	/* write license number */
//	temp_arr[0] = '1';temp_arr[1] = '2';temp_arr[2] = '3';temp_arr[3] = '4';temp_arr[4] = '5',temp_arr[5] = '6', temp_arr[6] = '7';temp_arr[7] = '8';temp_arr[8] = '9';temp_arr[9] = 'A';
//	temp_arr[10] = '1';temp_arr[11] = '2';temp_arr[12] = '3';temp_arr[13] = '4';temp_arr[14] = '5',temp_arr[15] = '6', temp_arr[16] = '7';temp_arr[17] = '8';temp_arr[18] = '9';temp_arr[19] = 'B';
//	temp_arr[20] = '1';temp_arr[21] = '2';temp_arr[22] = '3';temp_arr[23] = '4';
//	temp_arr[0] = 'P';temp_arr[1] = 'W';temp_arr[2] = 'E';temp_arr[3] = '5';temp_arr[4] = '5',temp_arr[5] = '1', temp_arr[6] = '3';temp_arr[7] = '4';temp_arr[8] = '0';temp_arr[9] = '6';
//	temp_arr[10] = '5';temp_arr[11] = '0';temp_arr[12] = '3';temp_arr[13] = 'B';temp_arr[14] = '5',temp_arr[15] = '4', temp_arr[16] = 'F';temp_arr[17] = '8';temp_arr[18] = 'E';temp_arr[19] = 'O';
//	temp_arr[20] = 'F';temp_arr[21] = 'B';temp_arr[22] = 'R';temp_arr[23] = 'L';
	
//	atul temp_arr[0] = '9';temp_arr[1] = 'A';temp_arr[2] = 'A';temp_arr[3] = 'I';temp_arr[4] = 'B',temp_arr[5] = '5', temp_arr[6] = '4';temp_arr[7] = 'O';temp_arr[8] = 'B';temp_arr[9] = 'F';
//	temp_arr[10] = 'O';temp_arr[11] = 'F';temp_arr[12] = 'B';temp_arr[13] = 'S';temp_arr[14] = 'K',temp_arr[15] = 'D', temp_arr[16] = 'F';temp_arr[17] = '6';temp_arr[18] = '9';temp_arr[19] = 'K';
//	temp_arr[20] = '3';temp_arr[21] = '4';temp_arr[22] = '0';temp_arr[23] = 'Z';
	strcpy(temp_arr,"OFBSK8E5A8340155JB54O3FP");
	
	eeprom_data_read_write(LICENSE_NUM_INFO_ADDR, WRITE_OP, &temp_arr[0], 24);	   /* write license num */
	
	/* write serial number */
//	temp_arr[0] = '0';temp_arr[1] = '0';temp_arr[2] = '1';temp_arr[3] = '5';temp_arr[4] = 'A',temp_arr[5] = 'M', temp_arr[6] = 'F';temp_arr[7] = '2';temp_arr[8] = '1';temp_arr[9] = '0';
//	temp_arr[10] = '0';temp_arr[11] = '0';temp_arr[12] = '2';temp_arr[13] = '1';

//atul	temp_arr[0] = '0';temp_arr[1] = '0';temp_arr[2] = '1';temp_arr[3] = '6';temp_arr[4] = 'A',temp_arr[5] = 'M', temp_arr[6] = 'F';temp_arr[7] = '2';temp_arr[8] = '1';temp_arr[9] = '0';
//	temp_arr[10] = '0';temp_arr[11] = '1';temp_arr[12] = '9';temp_arr[13] = '9';

	strcpy(temp_arr,"16AMF2100091");
	eeprom_data_read_write(SERIAL_NUM_INFO_ADDR, WRITE_OP, &temp_arr[0], SERIAL_NUM_LEN);	   /* write serial num */	

	/* */
	temp_arr[0] = 3;temp_arr[1] = 7;temp_arr[2] = 14;							
	eeprom_data_read_write(CURRENT_FW_ADDR, WRITE_OP, &temp_arr[0], VERSION_NUM_LEN);	   /* write version num */
	temp_arr[0] = 0;temp_arr[1] = 0;temp_arr[2] = 0;							
	eeprom_data_read_write(UPGRADING_FW_ADDR, WRITE_OP, &temp_arr[0], VERSION_NUM_LEN);	   /* write updating num */	
	
	
 
 	
}


void clear_registration_log(void){
	uint8_t loc, temp_arr[5], count;

	loc	= 'N';
	eeprom_data_read_write (SYSTEM_INFO_ADDR, WRITE_OP, &loc, 1);		   	/* board status */
	temp_arr[0] = '\0';
	for(count = 0; count < TOTAL_REG_KEY; count++){
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , WRITE_OP, &temp_arr[0], 1);
	}

}

/********************************************************************************************************************************
 * Function name: 	void write_default_configuration(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	4 may,2014
 *
 * Date modified:	4 may,2014
 *
 * Description: 	This function writes default configuration of WMS.
 *
 * Notes:
 *******************************************************************************************************************************/
void write_default_configuration(void){
	uint8_t temp_arr[200],loc, tank_pump_setting_configured; //, license_num[10] = {1,2,3,4,5,6,7,8,9,0};
	uint32_t count;
	uint8_t gmt_standard[30] = "                              ";  //used in set rtc
	
	

	/* clear device registration information */
	temp_arr[0] = '\0';
	for(count = 0; count < TOTAL_REG_KEY; count++){
		temp_arr[0] = '0';
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, WRITE_OP,&temp_arr[0], 1);
	//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 1, &temp_arr[0], 1);	
		temp_arr[0] = '\0';
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 17,WRITE_OP, &temp_arr[0], 1);
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 27, WRITE_OP, &temp_arr[0], 1);
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, READ_OP,&temp_arr[0], 1);
	}
	for(count = 0; count < TOTAL_REG_KEY; count++){
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, WRITE_OP, &temp_arr[0], DEVICE_REG_LEN);	
	}
	/**************************************/
		

	
	/* write tank & pump settings */
	write_def_tank_pump_settings();			   //  tank setting atul

	/* write default water usage settings */
	for(count = 0; count < 7; count++){
		auto_task_info.tsk_enable_flag[count] = 0;	
	}
	for(count = 7; count < 9; count++){						/* inherent task */
		auto_task_info.tsk_enable_flag[count] = 1;	
	}
	
	auto_task_info.tsk_enable_flag[0] = 1;		//atul  [pump off at dry run till 10 Minutes]
	auto_task_info.tsk_enable_flag[7] = 1;    //atul  [pump off at tank full task at sensor 100%]
	
	auto_task_info.tsk_1_preset_level = 10;
	auto_task_info.tsk_2_preset_level = 15;
//	auto_task_info.tsk_3_preset_level = 15;
	auto_task_info.tsk_4_preset_level = 25;
	auto_task_info.tsk_5_preset_level = 30;
	auto_task_info.tsk_6_preset_level[0] = 100;
	auto_task_info.tsk_6_preset_level[1] = 70;
	auto_task_info.tsk_7_preset_level[0] = 30;
	auto_task_info.tsk_7_preset_level[1] = 60;
	auto_task_info.tsk_7_preset_level[2] = 75;
	auto_task_info.tsk_7_preset_level[3] = 50;
//	auto_task_info.tsk_7_preset_level[4] = 100;
//	auto_task_info.tsk_9_preset_level = 15;
	eeprom_data_read_write(WATER_USAGE_INFO_START_ADDR, WRITE_OP, (uint8_t *)&auto_task_info.tsk_enable_flag[0], WATER_USAGE_LEN);	
	


	/****************************************************/
	/* clear event logs */
	temp_arr[0] = 0;
	#ifdef EVENT_ENABLE
	eeprom_data_read_write (TOTAL_EVENT_LOG_COUNT, WRITE_OP, &temp_arr[0], 1);
	for(count = 0; count < MAX_EVENT; count++){
		eeprom_data_read_write(TANK_PUMP_EVENT_START_ADDR + TANK_PUMP_RECORD_LEN*count, WRITE_OP, temp_arr, TANK_PUMP_RECORD_LEN);
	}	
	#endif

	#ifdef NOTIFICATION_ENABLE
	
	eeprom_data_read_write (TOTAL_NOTIFICATION_LOG_COUNT, WRITE_OP, &temp_arr[0], 1);
	for(count = 0; count < MAX_EVENT; count++){
		eeprom_data_read_write(NOTIFICATION_START_ADDR + NOTIFICATION_LEN*count, WRITE_OP, temp_arr, NOTIFICATION_LEN);
	}	
	#endif
//	 save_default_schedules();

 	buzzer_switch_info.buzzer_activate_flag = 1;
	buzzer_switch_info.buzzer_snooze_dur = 30;
  buzzer_set_level = 30;
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR, WRITE_OP, (uint8_t *)&buzzer_switch_info.buzzer_activate_flag, 1);			/* buzzer activation flag */			
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 1, WRITE_OP, (uint8_t *)&buzzer_switch_info.buzzer_snooze_dur, 1);			/* buzzer snooze duration */
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 2, WRITE_OP, (uint8_t *)&buzzer_set_level, 1);								/* buzzer blow level */
	system_info.dhcp_flag = 1;
	eeprom_data_read_write(DHCP_INFO_ADDR, WRITE_OP, &system_info.dhcp_flag, 1);

	/* write admin password */
//	temp_arr[0] = '1';temp_arr[1] = '2';temp_arr[2] = '3';temp_arr[3] = '4';temp_arr[4] = '5',temp_arr[5] = '6';
	strcpy(temp_arr,"1admin     123456      ");
	eeprom_data_read_write(DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE), WRITE_OP, &temp_arr[0], ADMIN_USER_PWD_MAXLEN);
	
	tank_pump_setting_configured = 0;
	eeprom_data_read_write(TANK_PUMP_SENSOR_SETTING_ADDR, WRITE_OP,&tank_pump_setting_configured, 1); 
	
	eeprom_data_read_write(GMT_STANDARD_ADDRESS, WRITE_OP, &gmt_standard[0], 30);  //write 30 space at GMT_STANDARD_ADDRESS

}
/********************************************************************************************************************************
 * Function name: 	void clear_consumption_logs(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	17 june,2014
 *
 * Date modified:	17 june,2014
 *
 * Description: 	This function clears the consumption logs.
 *
 * Notes:
 *******************************************************************************************************************************/
//
//void clear_consumption_logs(void){
//	 uint8_t temp_arr[200], *addr_ptr;
//	 uint32_t  count, look_up_addr;
//
//	addr_ptr = (uint8_t *)&look_up_addr;
///*/	
//   // clear consumption logs */
//	/* clear OHT logs */
//	 for(count = 0; count < 200; count++){
//		temp_arr[count] = 0;
//	}
//	total_oht_consumption_logs = 0;
//	dflash_write_multiple_byte (TOTAL_OHT_TANK_CONSUMPTION_ADDR, (uint8_t*)&total_oht_consumption_logs, 1); 
//	for(count = 0; count < NO_OF_DAYS; count++)
//		dflash_write_multiple_byte(OHT_TANK_CONSUMPTION_START_ADDR + ONE_DAY_CONSUMPTION_LEN*count, &temp_arr[0], ONE_DAY_CONSUMPTION_LEN);
//	
//	/* Refresh look up table */
//	 for(count = 0; count < NO_OF_DAYS; count++){
//		look_up_addr = OHT_TANK_CONSUMPTION_START_ADDR + ONE_DAY_CONSUMPTION_LEN*count;
//		dflash_write_multiple_byte(OHT_CONSUMPTION_LOOK_UP_START_ADDR + count*LOOK_UP_LEN, (uint8_t *)addr_ptr, LOOK_UP_LEN);
//	}
//	dflash_write_multiple_byte(TOTAL_OHT_LOOK_UP_ENTRY, &temp_arr[0], 1);    // initially total log  = 0
//
//	 /* clear UGT logs */
//	total_ugt_consumption_logs = 0;
//	dflash_write_multiple_byte (TOTAL_UGT_TANK_CONSUMPTION_ADDR, (uint8_t*)&total_ugt_consumption_logs, 1); 
//	for(count = 0; count < NO_OF_DAYS; count++)
//		dflash_write_multiple_byte(UGT_TANK_CONSUMPTION_START_ADDR + ONE_DAY_CONSUMPTION_LEN*count, &temp_arr[0], ONE_DAY_CONSUMPTION_LEN);
//	/* Refresh look up table */
//	 for(count = 0; count < NO_OF_DAYS; count++){
//		look_up_addr = UGT_TANK_CONSUMPTION_START_ADDR + ONE_DAY_CONSUMPTION_LEN*count;
//		dflash_write_multiple_byte(UGT_CONSUMPTION_LOOK_UP_START_ADDR + count*LOOK_UP_LEN, (uint8_t *)addr_ptr, LOOK_UP_LEN);
//	}
//	dflash_write_multiple_byte(TOTAL_UGT_LOOK_UP_ENTRY, &temp_arr[0], 1); 
//
//}

/********************************************************************************************************************************
 * Function name: 	void restore_configured_settings()
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		Nikhil Kukreja
 *
 * Date created: 	17 june,2014
 *
 * Date modified:	17 june,2014
 *
 * Description: 	This function restores the configured settings of system.
 *
 * Notes:
 *******************************************************************************************************************************/

void restore_configured_settings(){
	uint8_t count, temp = 0, loc1;

//	oht_immed.pump_state = OHT_pump_state;
//	oht_immed.year       = last_pump_triggered_date_OHT.RTC_Year;
//	oht_immed.month		 = last_pump_triggered_date_OHT.RTC_Month;
//	oht_immed.date		 = last_pump_triggered_date_OHT.RTC_Date;
//	oht_immed.hour		 = last_pump_triggered_time_OHT.RTC_Hours;
//	oht_immed.min		 = last_pump_triggered_time_OHT.RTC_Minutes;
//
//	ugt_immed.pump_state = UGT_pump_state;
//	ugt_immed.year       = last_pump_triggered_date_UGT.RTC_Year;
//	ugt_immed.month		 = last_pump_triggered_date_UGT.RTC_Month;
//	ugt_immed.date		 = last_pump_triggered_date_UGT.RTC_Date;
//	ugt_immed.hour		 = last_pump_triggered_time_UGT.RTC_Hours;
//	ugt_immed.min		 = last_pump_triggered_time_UGT.RTC_Minutes;

	
	eeprom_data_read_write (SYSTEM_INFO_ADDR, READ_OP, &system_info.status, 1);		   	/* board status */
	eeprom_data_read_write (SYSTEM_MAC_ID_INFO_ADDR, READ_OP, &mac_adr[0], 6);	/* system mac id */
	for(count = 0; count < 6; count++, temp += 2){
		system_info.mac_id[temp] =	 ((mac_adr[count] & 0xf0) >> 4) + 0x30;
		system_info.mac_id[temp + 1] =	 (mac_adr[count] & 0x0f) + 0x30;
	}
	for(temp = 0; temp < 12; temp++){
		if(system_info.mac_id[temp] > 0x39){
			system_info.mac_id[temp] += 7;
		}
	}
	eeprom_data_read_write(TOTAL_REG_KEY_ADDR, READ_OP, &total_client_license, 1);	   /* read license class */
	eeprom_data_read_write (LICENSE_NUM_INFO_ADDR, READ_OP, &system_info.license_num[0], 24);	 /* license number */
 	eeprom_data_read_write (SERIAL_NUM_INFO_ADDR, READ_OP, &system_info.serial_num[0], SERIAL_NUM_LEN);	 /* license number */
 	eeprom_data_read_write(DHCP_INFO_ADDR, READ_OP, &system_info.dhcp_flag, 1);
	
	if(system_info.dhcp_flag == 0){
		eeprom_data_read_write(NW_INFO_ADDR, READ_OP, &system_info.nw_setting[0], 16);		
	//	dhcp_disable ();
		for(loc1 = 0; loc1 < 4; loc1++, temp++){
			temp = system_info.nw_setting[loc1 + 4];	
			system_info.nw_setting[loc1 + 4] = system_info.nw_setting[loc1 + 8];  
			system_info.nw_setting[loc1 + 8] = temp;
		}		
						
	}
	check_licencse_authenticate_logic((char *)&system_info.license_num[0], (char *)&system_info.serial_num[0], (char *)&system_info.mac_id[0]);
//	dflash_read_multiple_byte(CURRENT_FW_ADDR, (uint8_t *)&system_info.fw_ver[0], VERSION_NUM_LEN);	   /* write license num */		
//	license_class = 3;
	total_client_license = license_class;
	memcpy((uint8_t *)&system_info.fw_ver[0], &Neotech_Header[6], VERSION_NUM_LEN); 
	read_tank_pump_settings();    // pending
	
	eeprom_data_read_write(WATER_USAGE_INFO_START_ADDR, READ_OP, (uint8_t *)&auto_task_info.tsk_enable_flag[0], WATER_USAGE_LEN);	
	auto_task_info.tsk_enable_flag[7] = 1;		  // inherent task
	auto_task_info.tsk_enable_flag[8] = 1;		  // inherent task

	auto_task_pump_state[2].oht_pump = NOT_DEFINE;
	auto_task_pump_state[2].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[3].oht_pump = OFF;
	auto_task_pump_state[3].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[4].oht_pump = OFF;
	auto_task_pump_state[4].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[5].oht_pump = ON;
	auto_task_pump_state[5].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[6].oht_pump = ON;
	auto_task_pump_state[6].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[7].oht_pump = OFF;
	auto_task_pump_state[7].ugt_pump = NOT_DEFINE;	 

	auto_task_pump_state[8].oht_pump = NOT_DEFINE;
	auto_task_pump_state[8].ugt_pump = OFF;	 

	/* read buzzer setting */
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR, READ_OP, (uint8_t *)&buzzer_switch_info.buzzer_activate_flag, 1);			/* buzzer activation flag */			
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 1, READ_OP, (uint8_t *)&buzzer_switch_info.buzzer_snooze_dur, 1);			/* buzzer snooze duration */
	buzzer_switch_info.buzzer_snooze_dur *= 60;
	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 2, READ_OP, (uint8_t *)&buzzer_set_level, 1);


	/* Read immediate setting */
	eeprom_data_read_write(IMMEDIATE_DATA, READ_OP, (uint8_t *)&oht_immed, 7);
	eeprom_data_read_write(IMMEDIATE_DATA + 7, READ_OP, (uint8_t *)&ugt_immed, 7);

	/* read immediate setting */
//	eeprom_data_read_write(IMMEDIATE_DATA, READ_OP, (uint8_t *)&oht_immed.pump_state, 7);
//	status.oht_pump_ptr->pump_state = oht_immed.pump_state;
//	//for(temp = 0; temp < 5; temp++){
//		status.oht_pump_ptr->last_on_off_time[0]	= oht_immed.year;
//		status.oht_pump_ptr->last_on_off_time[1]	= oht_immed.month;
//		status.oht_pump_ptr->last_on_off_time[2]	= oht_immed.date;
//		status.oht_pump_ptr->last_on_off_time[3]	= oht_immed.hour;
//		status.oht_pump_ptr->last_on_off_time[4]	= oht_immed.min;
//		status.oht_pump_ptr->last_on_off_time[5]	= 0;
//	//}

//	memcpy((uint8_t *)&status.oht_pump_ptr->last_on_off_time, (uint8_t *)&oht_immed.year, 5);
//	if(wms_sys_config.total_ugt > 0){
//		eeprom_data_read_write(IMMEDIATE_DATA + 7, READ_OP, (uint8_t *)&ugt_immed.pump_state, 7);
//		memcpy((uint8_t *)&status.oht_pump_ptr->last_on_off_time, (uint8_t *)&ugt_immed.year, 5);
//	}	 
	
//	eeprom_data_read_write (TOTAL_OHT_TANK_CONSUMPTION_ADDR, READ_OP, (uint8_t*)&total_oht_consumption_logs, 1); 
//	read_current_hour_oht_consumption();
//	if(wms_sys_config.total_ugt > 0){
//		eeprom_data_read_write (TOTAL_UGT_TANK_CONSUMPTION_ADDR, READ_OP, (uint8_t*)&total_ugt_consumption_logs, 1); 
//		read_current_hour_ugt_consumption();
//	}
//	extract_device_reg_info();		/* gui info */

   

}



/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


 




/************************************************************************//**
* @file			packet.c
*
* @brief		This module contains the Serial communication Data packets.
*
* @attention	Copyright 2012 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 20/05/12 \n by \em Aman Deep
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			This module mainly consists of the communication packet information.
*				
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include <stdint.h>
#include <stdlib.h>
#include "port_mapping.h"
#include "pin_config.h"
#include "packet.h"	
#include "uart.h"
#include "registration.h"
#include "memory_info.h"
#include "eeprom.h"
#include "core_cm0.h"
#include "timer.h"
#include "system.h"

#include "std_periph_headers.h"

/*
**===========================================================================
**		Global Variable Declaration Section
**		AVOID DECLARING ANY
**===========================================================================
*/ 
serialport usart1, usart2;

struct local_packet	packet_frame_add, out_frame;
struct local_packet	*PTRpacket_frame, *PTRout_frame;
//extern struct system_params sys_info;
//extern struct led_params led_info;

struct system_params sys_info;

extern uint8_t uart_tx_flag;

frame_fields(frame_byte)
priority_fields(prio_field_byte)

		/**********************************************************/
		/*				Temp variables for testing				  */
		/**********************************************************/
/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/
/* Validate Serial Port0 Receive buffer data */
void two_byte_status_message_in(uint8_t);

/* Packets more than 2 bytes */
void packets_more_than_2bytes_in(serialport *uart_no);

/* Transmit Two Byte Status Message */
void two_byte_status_message_out(void);		 	

/* Validate packet recieved. */
void start_validation_decide_packet_bytes_in(serialport *uart_no);

void analog_output(uint8_t port_no);

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
{
	uint8_t crc4 = 0, temp_var, *crc_check_packets;
	crc_check_packets = (void*)field;
	crc_check_packets++;
	for(temp_var = 0; temp_var < nob; temp_var++)
	{
		crc4 ^= *crc_check_packets;
		crc_check_packets++;
	}
	temp_var = ((crc4 & GET_MSB_DATA) >> 4);
	crc4 = ((crc4 & GET_LSB_DATA) ^ temp_var);
	return crc4;
}

/************************************************************************//**
*					uint16_t get_rxgetptr_data(void)
*
* @brief			This routine is used to get the valid data from the receive 
*					buffer & if data byte is not valid return a negative value.
*
* @param 			None.
* @param 			None.
*
* @returns			Data byte if valid & negative no. if not valid.
*
* @exception		None.
*
* @author			Aman Deep
* @date				07/06/11
* @note				None.                                       
****************************************************************************/
uint16_t get_rxgetptr_data(serialport *uart_no)
{
	volatile uint16_t waitcount;
	uint8_t byte_fetched = 0;
	signed int fetch_data_byte = 0;
		
	if ((uart_no->RxGetPtr != uart_no->RxPutPtr)) {
		/* atleast one byte has been received from uart */
		fetch_data_byte = *uart_no->RxGetPtr;			  
		fetch_data_byte &= CHK_VALID_DATA;
		uart_no->RxGetPtr++;
		/*If read position is above last position restore it to base position */
		if ((uart_no->RxGetPtr >= (uart_no->InBuf + RX_BUF_LEN))) {
			uart_no->RxGetPtr = uart_no->InBuf;		
		}
	}
  	/* Either the buffer is empty or full */
	else {
		/* If buffer state is empty & packet processing is in progress, wait for 3 bytes interval (260 us for 115200 baudrate) */
		/* Make sure no switch over happen during the waiting interval */		
		for (waitcount = 0; waitcount < 4000; waitcount++) { 
			if (uart_no->RxGetPtr != uart_no->RxPutPtr) {
				
				fetch_data_byte = *(uart_no->RxGetPtr);
				fetch_data_byte &= CHK_VALID_DATA;
				uart_no->RxGetPtr++;
				/*If read position is above last position restore it to base position */
				if ((uart_no->RxGetPtr >= (uart_no->InBuf + RX_BUF_LEN))) {		
					uart_no->RxGetPtr = uart_no->InBuf;
				}

				byte_fetched = 1;					
				break;
			}
		}	// 1 loop = 50ns		
		/* If after waiting period any byte is received, return it else return invalid byte */
		if (byte_fetched == 0) {
			fetch_data_byte = NEG_ONE;
			fetch_data_byte &= INVALID_DATA;				
		}
	}	
	return fetch_data_byte;
}		   

/************************************************************************//**
*								void restore_ptr_if_invalid_data(uint8_t *restore_getpointer, uint8_t getptr_after_timeout, uint8_t *uart_no)
*
* @brief						This routine is used to To restore the pointer if data is invalid 
*								invalid.
*
* @param *restore_getpointer	Pointer to the structure that contains data.
* @param getptr_after_timeout	No. of data bytes for which crc is to be calculated.
*
* @returns						None.
*
* @exception					None.
*
* @author						Aman Deep
* @date							24/06/11
* @note							Edited by Praveen: changed in restore pointer when buffer is at starting position.
****************************************************************************/
void restore_ptr_if_invalid_data(uint8_t *restore_getpointer, uint8_t getptr_after_timeout, serialport *uart_no){
	uint8_t *restore_getptr;

	restore_getptr = restore_getpointer;

	if(restore_getptr >= (uart_no->InBuf + 2))
		uart_no->RxGetPtr = (restore_getptr - 2);
	else if(restore_getptr == (uart_no->InBuf + 1))
		uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
	else if(restore_getptr == uart_no->InBuf)
		uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 2);
}


/************************************************************************//**
*					void start_validation_decide_packet_bytes_in(void)
*
* @brief			This routine is used to validate the start byte in packet &  
*					decide packet bytes.
*
* @param 			None.
* @param 			None.
*
* @brief			This function validate packets start if not then handle it,
*					make distinguish between 2 bytes or more bytes packet, 
*					compare slave address if matched executes 2bytes packets 
*					& fill data in queues >= 4bytes packets.
*
* @returns			None.
*
* @exception		None.
*
* @author			Aman Deep
* @date				30/05/11
* @note				None.                                       
****************************************************************************/
void start_validation_decide_packet_bytes_in(serialport *uart_no) {
	uint8_t temp_var = 0/*, status_id = 0*/;
	uint16_t rxgetptr_data;
	
	rxgetptr_data = get_rxgetptr_data(uart_no);					//get next byte data
	if(rxgetptr_data != INVALID_DATA){							//check the data is valid
		/* Check valid start till buffer get exhausted or for a valid start */
		while((VALIDATE_START(rxgetptr_data) != TRUE)||(uart_no->RxGetPtr !=(uart_no->InBuf + RX_BUF_LEN + 1))){
			if(VALIDATE_START(rxgetptr_data) == TRUE){
				///////start++;
				temp_var = FRAME_TWO_OR_MORE_BYTE(rxgetptr_data);		//check packet for two or more byte
				if(temp_var == FALSE){
					PTRpacket_frame -> start_byte = RX_CRC(rxgetptr_data);	//if 4 bytes, then get crc
				}
					
				break;
			} //if(VALIDATE_START(rxgetptr_data) == TRUE)
			else{
				rxgetptr_data = get_rxgetptr_data(uart_no);				//get next byte data
				if(rxgetptr_data != INVALID_DATA){						//check valid data byte
					if((uart_no->RxGetPtr >= (uart_no->InBuf + RX_BUF_LEN))||(uart_no->RxGetPtr == uart_no->RxPutPtr)){			//closed bcoz one packet neglect after one execution
						if(uart_no->RxGetPtr != uart_no->InBuf)
							uart_no->RxGetPtr = (uart_no->RxGetPtr - 1);
						else
							uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
						return;
					}
				}
				if(rxgetptr_data == INVALID_DATA)									//check valid data byte
				return;
			}
		}				
		/* save slave address, execute 2 byte packet & enqueue more than 2 byte packet*/
		rxgetptr_data = get_rxgetptr_data(uart_no);		 							//get next byte data
		if(rxgetptr_data != INVALID_DATA){											//check valid data byte
			PTRpacket_frame -> slave_add = rxgetptr_data;
			if(temp_var == TRUE){
				if(PTRpacket_frame -> slave_add == BROADCAST_SLAVE_ADDRESS){
//					load_registeration_request_packet_in_outbuff();
				}
				else {
					if(uart_no->RxGetPtr != uart_no->InBuf)				// If the Packet was not for us
						uart_no->RxGetPtr = (uart_no->RxGetPtr - 1);	// then restore pointer as we do in crc
					else											  	// mismatched
						uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
				}
				return;
			}
			else{												//more than 2 bytes Data Frame Identified
				packets_more_than_2bytes_in(uart_no);
    		}
		}//if(rxgetptr_data != 0xFFFF)
	}//if(rxgetptr_data != 0xFFFF)
	return;	 
}
  
/************************************************************************//**
*					void packets_more_than_2bytes_in()
*
* @brief			This routine is used to performs rx 4byte packet execution &   
*					> 4byte packet saving in priority queues.
*
* @param 			None.
* @param 			None.
*
* @returns			None.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
void packets_more_than_2bytes_in(serialport *uart_no) {
	uint8_t calc_data_crc, temp_var = 0, nob = 3, *ptr_packet_frame, *restore_getptr, not_req_crc_chk = 0, getptr_after_timeout = 0;
	uint16_t rxgetptr_data;

	ptr_packet_frame = (void*)PTRpacket_frame;

	//Ptr restoration value if data corrupted or in waiting
	restore_getptr = uart_no->RxGetPtr;
	rxgetptr_data = get_rxgetptr_data(uart_no);
	if(rxgetptr_data != INVALID_DATA)											//check valid data byte
	{
		getptr_after_timeout++;													//if data byte is valid
		PTRpacket_frame -> port_no = PORT_NO(rxgetptr_data);					//Get port no. from received packet & save it
		rxgetptr_data = get_rxgetptr_data(uart_no);
		if(rxgetptr_data != INVALID_DATA)										//check valid data byte
		{
			getptr_after_timeout++;
			frame_byte.status_4byte.frame_type = FRAME_TYPE(rxgetptr_data);					//frame bits
				
			switch(frame_byte.status_4byte.frame_type)										//check packet length
			{
				//4 byte status message execute if CRC & slave address matched
				case 0:
						frame_byte.status_4byte.status = STATUS_5BITS(rxgetptr_data);			//status bits
						PTRpacket_frame -> frame_byte = frame_byte.val;				 			//save frame byte
						calc_data_crc = calc_crc4(PTRpacket_frame, nob);
						if(PTRpacket_frame -> start_byte == calc_data_crc)
						{																							// CASE 0 is to be discussed.
							if(PTRpacket_frame->slave_add == sys_info.slave_add)
							{
								//four_byte_status_message_in(frame_byte.status_4byte.status);
								//slave add matched, execute 4 byte status message
							}
							else
							break;
						}
						else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
						{
							//invalid data, next data is pending
							restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
						}
						else
						//crc mismatched, data corrupted, check next start byte
						uart_no->RxGetPtr = (restore_getptr - 1);
						break;
						//4 byte I/O message execute if CRC & slave address matched
				case 1:
						////case++;
						frame_byte.cmnd_param_4byte.command = COMMAND_4BYTE(rxgetptr_data);		//command bits
						frame_byte.cmnd_param_4byte.parameter = PARAMETER_4BYTE(rxgetptr_data);	//parameter bits
						PTRpacket_frame -> frame_byte = frame_byte.val;				 			//save frame byte
						calc_data_crc = calc_crc4(PTRpacket_frame, nob);
						if(PTRpacket_frame -> start_byte == calc_data_crc)
						{
							if (PTRpacket_frame->slave_add == sys_info.slave_add) {
//								if (PTRpacket_frame->port_no >= 64 && PTRpacket_frame->port_no <= 95) {
//									set_do (PTRpacket_frame->port_no, ((PTRpacket_frame->frame_byte == 0x38)? 1 : 0));
//									PTRout_frame->slave_add = sys_info.slave_add;
//									PTRout_frame->port_no = PTRpacket_frame->port_no;
//									PTRout_frame->frame_byte = PTRpacket_frame->frame_byte;
//									tx_out_packet(uart_no, PTRout_frame, 3);
//								}
							}
						}
						else
						if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr)){
							//invalid data, restore packet
							restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
						}
						else{
							//crc mismatched, data corrupted, check next start byte
							if(uart_no->RxGetPtr != uart_no->InBuf)
							uart_no->RxGetPtr = (uart_no->RxGetPtr - 1);
							else
							uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
						}
						break;
						
						
						//Message > = 6 bytes & < = 12 bytes enqueue if crc & slave address is matched
				case 2:			// Receives the new firmware upgrade packet in this case. 
						frame_byte.frame_length_rfu_6or_more_bytes.rfu = RFU(rxgetptr_data);
						frame_byte.frame_length_rfu_6or_more_bytes.length = PACKET_LENGTH(rxgetptr_data);
						PTRpacket_frame -> frame_byte = frame_byte.val;
						rxgetptr_data = get_rxgetptr_data(uart_no);
						if(rxgetptr_data != INVALID_DATA)								//check valid data byte
						{
							getptr_after_timeout++;
							prio_field_byte.stscmnd_type_prio_packet.priority = PRIORITY(rxgetptr_data);//priority bits
							prio_field_byte.stscmnd_type_prio_packet.type = TYPE(rxgetptr_data);		//type bits 
							prio_field_byte.stscmnd_type_prio_packet.status_cmnd = STATUS_CMND(rxgetptr_data);
							PTRpacket_frame -> prio_stus_cmnd_byte = prio_field_byte.val;				//save priority byte
							ptr_packet_frame++;
							
							if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x00)					//6 byte packet
							{
								nob = 5;							
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x01)			//8 byte packet
							{
								nob = 7;								
							}
							if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x02)					//10byte packet
							{
								nob = 9;								
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x03)			//12byte packet
							{
								nob = 11;								
							}
							ptr_packet_frame = ptr_packet_frame + 4;
							for(temp_var=0; temp_var < nob-4; temp_var++)
							{
								rxgetptr_data = get_rxgetptr_data(uart_no);
								if(rxgetptr_data != INVALID_DATA)										//check valid data byte
								{
										*ptr_packet_frame = rxgetptr_data;
										ptr_packet_frame++;
										getptr_after_timeout++;
								}
								else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
								{
									//incomplete msg, restore packet
									restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
									not_req_crc_chk = 1;
									break;
								}
							}//for(temp_var=0; temp_var < nob-4; temp_var++)
							if(not_req_crc_chk == 0)
							{
								calc_data_crc = calc_crc4(PTRpacket_frame, nob);
								if(PTRpacket_frame -> start_byte == calc_data_crc)
								{
									if((PTRpacket_frame -> slave_add == sys_info.slave_add) || (PTRpacket_frame -> slave_add == BROADCAST_SLAVE_ADDRESS)) {
						//--------------------------------------------------------------------------------------------------------------------				
										if (PTRpacket_frame -> port_no == SYSTEM_SPECIFIC_PORT) {
//											uint8_t fw_ver [3];
//											eeprom_data_read_write(CURR_FW_VER_ADD, READ_OP, fw_ver, sizeof (fw_ver));
//											if (!((fw_ver [0] == PTRpacket_frame->data_byte[0]) && (fw_ver [1] == PTRpacket_frame->data_byte[1]) && (fw_ver [2] == PTRpacket_frame->data_byte[2]))) {
//												uint8_t ack_pkt [2] = {0xB5};
//												ack_pkt [1] = sys_info.slave_add;
//												while (uart_no->TxPutPtr != uart_no->TxGetPtr); // wait for tx buffer to get empty
//												eeprom_data_read_write(CURR_FW_VER_ADD, WRITE_OP, PTRpacket_frame->data_byte, sizeof (fw_ver));
//												sys_info.snd_reg_pkt_flg = 1;
//												if (sys_info.slave_add - 1) {
//													timer_init(TIM3, TIME_MILLI_SEC, ((sys_info.slave_add - 1) * 30)); // send ack to HC after this amount of time.
//													TIM_Cmd(TIM3, ENABLE);
//													while (sys_info.snd_reg_pkt_flg);
//												}
//												
//												//if(uart_tx_flag)					// BY PARVESH
//												 uart_send_str (USART1, ack_pkt, sizeof (ack_pkt));
//												
//												NVIC_SystemReset ();
//											}
										}
					//--------------------------------------------------------------------------------------------------------------------					
										else	if (PTRpacket_frame -> port_no == IO_SETTING_PORT) {
											switch (PTRpacket_frame -> prio_stus_cmnd_byte) {
												case 0:	/* Activity Setting. */
//														process_pir_activity_time_pkt ((uint8_t *)PTRpacket_frame -> data_byte);
														break;
											}
										} 
					//--------------------------------------------------------------------------------------------------------------------					
										else if (PTRpacket_frame->slave_add == BROADCAST_SLAVE_ADDRESS) {		// unregister the ecowave.
											if (PTRpacket_frame -> port_no == REGISTRATION_PN) {
												if (PTRpacket_frame -> prio_stus_cmnd_byte == 1) {
//													sys_info.slave_add = BROADCAST_SLAVE_ADDRESS;
//													eeprom_data_read_write(SELF_SLAVE_ADD, WRITE_OP, (uint8_t *)&sys_info.slave_add, 1);
//													led_info.sys_led_timeout = 2;
												}
											}
										}
					//--------------------------------------------------------------------------------------------------------------------						
									}
								}
								else		//crc mismatched, data corrupted,check next start byte
								{	//uart_no->RxGetPtr = (restore_getptr-1);
									if(uart_no->RxGetPtr != uart_no->InBuf)
									uart_no->RxGetPtr = (uart_no->RxGetPtr - 1);
									else
									uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
								}
							}//if(not_req_crc_chk == 0)
						} //if(rxgetptr_data != 0xFFFF)
						else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
						{
							//incomplete msg, restore packet
							restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
						}
						break;
						//Message > = 14 bytes enqueue if crc & slave address is matched
				case 3:
						frame_byte.frame_length_rfu_6or_more_bytes.rfu = RFU(rxgetptr_data);
						frame_byte.frame_length_rfu_6or_more_bytes.length = PACKET_LENGTH(rxgetptr_data);
						PTRpacket_frame -> frame_byte = frame_byte.val;
						rxgetptr_data = get_rxgetptr_data(uart_no);
						if(rxgetptr_data != 0xFFFF)													//check valid data byte
						{
							getptr_after_timeout++;
							prio_field_byte.stscmnd_type_prio_packet.priority = PRIORITY(rxgetptr_data);	//priority bits
							prio_field_byte.stscmnd_type_prio_packet.type = TYPE(rxgetptr_data); 			//type bits
							prio_field_byte.stscmnd_type_prio_packet.status_cmnd = STATUS_CMND(rxgetptr_data);//status bits
							PTRpacket_frame -> prio_stus_cmnd_byte = prio_field_byte.val;					//save priority byte
							ptr_packet_frame++;

							if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x00)				//14bytes packet
							{
								nob = 13;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.length==0x01)		//16bytes packet
							{
								nob = 15;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x00)			//18byte packet
							{
								nob = 17;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x01)			//20byte packet
							{
								nob = 19;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x02){			//22byte packet
								nob = 21;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x03)			//24byte packet
							{
								nob = 23;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x04){			//26byte packet
								nob = 25;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x05)			//28byte packet
							{
								nob = 27;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x06){			//30byte packet
								nob = 29;
							}
							else if(frame_byte.frame_length_rfu_6or_more_bytes.rfu==0x07)			//32byte packet
							{
								nob = 31;
							}
							ptr_packet_frame = ptr_packet_frame + 4;
							for(temp_var=0; temp_var < nob-4; temp_var++)
							{
								rxgetptr_data = get_rxgetptr_data(uart_no);
								if(rxgetptr_data != INVALID_DATA)									//check valid data byte
								{
									*ptr_packet_frame = rxgetptr_data;
									ptr_packet_frame++;
									getptr_after_timeout++;
								}
								else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
								{
									restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
									not_req_crc_chk = 1;
									break;
								}
							}//for(temp_var=0; temp_var < nob-4; temp_var++)
							if(not_req_crc_chk == 0) {
								calc_data_crc = calc_crc4(PTRpacket_frame, nob);
								if(PTRpacket_frame -> start_byte == calc_data_crc) {
									if (PTRpacket_frame -> slave_add == BROADCAST_SLAVE_ADDRESS) {			// registeration process of ecowave
										if(PTRpacket_frame -> port_no == REGISTRATION_PN) {
//											if (validate_uid_rcvd (&PTRpacket_frame -> data_byte [0])) {
//												sys_info.slave_add = PTRpacket_frame -> data_byte[12];
//												eeprom_data_read_write(SELF_SLAVE_ADD, WRITE_OP, (uint8_t *)&sys_info.slave_add, 1);
//												led_info.sys_led_timeout = 10;
////												COMM_LED_ON;
////												update_flash_info (PTRpacket_frame -> data_byte[12], DATA_TYPE_SLAVE_ADD);
//											}
										}
									}
								}
								else
								{
									if(uart_no->RxGetPtr != uart_no->InBuf)
									uart_no->RxGetPtr = (uart_no->RxGetPtr - 1);
									else
									uart_no->RxGetPtr = (uart_no->InBuf + RX_BUF_LEN - 1);
								}
							}//if(not_req_crc_chk == 0)
						}//if(rxgetptr_data != 0xFFFF)
						else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
						{
							restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
						}
						break;
			}//switch(f_type)
		}//if(rxgetptr_data != 0xFFFF)
		else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))			//incomplete msg, restore packet
		{
			restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
		}
	}//if(rxgetptr_data != 0xFFFF)
	else if((rxgetptr_data == INVALID_DATA) && (uart_no->RxGetPtr == uart_no->RxPutPtr))
	{
		restore_ptr_if_invalid_data(restore_getptr, getptr_after_timeout, uart_no);
	}
	return;
}

/************************************************************************//**
*					void tx_out_packet(uint8_t nob)
*
* @brief			This routine is used to Tx more than four byte message.
*
* @param nob		No. of bytes to be transmit.
*
* @returns			None.
*
* @exception		None.
*
* @author			Aman Deep
* @date				04/07/11
* @note				None.
****************************************************************************/
void tx_out_packet(serialport *usart, struct local_packet *out_frame_struct_ptr, uint8_t nob) {
	uint8_t temp_var, calc_crc_val, *ptr_out_frame, pkt_fill, shift_i;
	ptr_out_frame = (void*)out_frame_struct_ptr;

	for(shift_i = 0; shift_i < PSA_BUF_LEN - 1; shift_i++)
	usart->out_pkt_info_buf [shift_i] = usart->out_pkt_info_buf [shift_i + 1];
	usart->out_pkt_info_buf [PSA_BUF_LEN - 1].packet_start_address_backup = usart->TxPutPtr;
	usart->out_pkt_info_buf [PSA_BUF_LEN - 1].bytes_left = nob + 1;

	calc_crc_val = calc_crc4(out_frame_struct_ptr, nob);
	temp_var = PACKET_START | calc_crc_val;
	out_frame_struct_ptr -> start_byte = temp_var;

//	temp_var = prio_field_byte.stscmnd_type_prio_packet.priority;

	while(usart->usart_state == USART_TRANSMITTING);
	usart->usart_state = USART_FILLING;

	for(shift_i = 4; shift_i < 30; shift_i+= 2){
		if(nob < shift_i || nob == (shift_i + 1)){
			for(pkt_fill = 0; pkt_fill < shift_i || (nob == (shift_i + 1)); pkt_fill++){
				* usart->TxPutPtr = * ptr_out_frame;
				ptr_out_frame++;
				usart->TxPutPtr++;
				if(usart->TxPutPtr == (usart->OutBuf + TX_BUF_LEN)){
//					uartA1_tx_flag = 1;
					usart->TxPutPtr = usart->OutBuf;
				}
				if(nob == (shift_i + 1) && pkt_fill == (shift_i + 1))
				break;
			}
			break;
		}
	}

	out_frame_struct_ptr -> prio_stus_cmnd_byte = 0x00;
	usart->usart_state = USART_IDLE;
}

void ChkIfPacketReceived (serialport *usart) {
//	*(usart->RxPutPtr++) = 0xAA;					// packet for unregistering of ecowave
//	*(usart->RxPutPtr++) = 0xFF;
//	*(usart->RxPutPtr++) = 225;
//	*(usart->RxPutPtr++) = 0x40;
//	*(usart->RxPutPtr++) = 1;
//	*(usart->RxPutPtr++) = 0;
	if (usart->RxGetPtr != usart->RxPutPtr) {
		start_validation_decide_packet_bytes_in(usart);							// Validating Packets.
//		led_info.comm_led_timeout = 15;
	}
}

void init_packet_protocol (serialport *usart) {
	uint8_t idx;
	PTRpacket_frame = &packet_frame_add;
	PTRout_frame = &out_frame;
	usart->RxPutPtr = usart->RxGetPtr = usart->InBuf;
	usart->TxPutPtr = usart->TxGetPtr = usart->OutBuf;

	for(idx = 0; idx < PSA_BUF_LEN; idx++) {
		usart->out_pkt_info_buf[idx].packet_start_address_backup = usart->OutBuf;
		usart->out_pkt_info_buf[idx].bytes_left = 0;
	}
	usart->curr_out_pkt_info.packet_start_address_backup = usart->OutBuf;
}

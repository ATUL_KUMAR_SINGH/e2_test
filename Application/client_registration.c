/************************************************************************//**
* @file			client_registration.c
*
* @brief		Contains  the API for client registation.
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/5/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
//#include"ethernet_packet.h"
#include "client_registration.h"
//#include "AT45DB161D.h"
#include "target.h"
#include <string.h>
#include "memory_map.h"
#include"json_client.h"
#include "tcp.h"
#include "eeprom.h"
#include "buyers_n_client_reg.h"

/*
**===========================================================================
**		Defines section
**===========================================================================
*/

/*
**===========================================================================
**		Global Variable Declaration Section
**		
**=========================================================================== */
extern struct wms_packet gui_send;
extern struct sys_info system_info;
extern uint8_t json_send_arr[1000], *json_send_ptr, license_class, total_client_license;
struct client_info	http_user_info[TOTAL_HTTP_SESSION]; 
uint8_t total_reg_client, glbl_arr[10];
//extern struct json_struct	json_info;

/************************************************************************//**
*	uint8_t device_registration(struct gui_payload *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine is used to register the GUI client.
*				
*
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			status - 1  unsuccessfull
*							- 0 successfull
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
//uint8_t device_registration(struct json_struct *wms_payload_info, uint8_t num_bytes){
//	uint8_t	 status = 0x31,  loc_flag = 0, count = 0 , temp_arr[50], temp = 1, write_arr[80], total_length, key_match, mac_id_len, pos = 0xff;
//	uint8_t countr, temp_flag = 0, loc = 0, loc_temp = 0; 
//	switch(wms_payload_info->data[temp++]){
//		case SYSTEM_DEVICE_REGISTRATION:
//		case SYSTEM_DEVICE_REGISTRATION_SEND_SETTING:
//			temp += 2;			// skip " 
//			if(system_info.status == 'N'){
//				if(strncmp((char *)&system_info.serial_num[0], (char *)&wms_payload_info->data[temp], SERIAL_NUM_LEN)== 0){				// scan serial number	
//					temp += (SERIAL_NUM_LEN + 1);		// increment address at value serial num & " 
//					temp++;			// skip " 
//					if(strncmp((char *)&system_info.license_num[0], (char *)&wms_payload_info->data[temp], LICENSE_NUM_LEN)== 0){	   	/* validate license number */
//						temp += (LICENSE_NUM_LEN + 1);		// increment address at value license num & "   
//						temp_arr[0] = '1';temp_arr[1] = '2';temp_arr[2] = '3';temp_arr[3] = '4';	 // generate registration key 
//						temp_arr[4] = '1',temp_arr[5] = '2', temp_arr[6] = '1';temp_arr[7] = '2';
//						temp_arr[8] = '4';temp_arr[9] = '3', temp_arr[10] = '2';temp_arr[11] = '1';
//						/* write registration key */
//						eeprom_data_read_write(REG_KEY_INFO_ADDR, WRITE_OP,&temp_arr[0], 12);
////						for(countr = 0; countr < 3; countr++){
////							dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN + 1, &temp_arr[countr*4], 4);	
////						}					
//						count = 0;
//						write_arr[count++] = '1';			// status 
//						#ifdef FW_VER_GREATER_THAN_3_7_25
//						
//						#else
//							memcpy(&write_arr[count], temp_arr, REGISTRATION_KEY_LEN);  // registration key 
//							count += REGISTRATION_KEY_LEN;
//						#endif
//						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   // read device mac id  
//						temp += 	(total_length + 2);	
//						count += total_length;
//						mac_id_len = total_length;
//						
//					//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR, &write_arr[0], 1 + total_length); // write client info
//						eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR, WRITE_OP,&write_arr[0], 1 + total_length); 										
//	
//						count = 0;
//						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   // read name  
//						temp += 	(total_length + 2);	
//						count += total_length;
//						write_arr[count++] = '\0';
//					//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + 1 + mac_id_len, &write_arr[0], count); // write client info 										
//						eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + 1 + mac_id_len, WRITE_OP, &write_arr[0], count);

//						count = 0;
//						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   // read  phone number 
//						temp += 	(total_length + 2);	
//						count += total_length;
//						write_arr[count++] = '\0';
//						
//					//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + 1 + mac_id_len + NAME_LEN, &write_arr[0], count);  // write client info 	
//						eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + 1 + mac_id_len + NAME_LEN, WRITE_OP, &write_arr[0], count);									
//						//dflash_read_multiple_byte(REG_KEY_INFO_ADDR , &temp_arr[0], REGISTRATION_KEY_LEN*TOTAL_REG_KEY);
//						status = '0';
//						system_info.status = 'Y';
//					//	dflash_write_multiple_byte (SYSTEM_INFO_ADDR, (uint8_t *)&system_info.status, 1);		   	// board status 
//						eeprom_data_read_write(SYSTEM_INFO_ADDR, WRITE_OP, (uint8_t *)&system_info.status, 1);

//					//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR, &temp_arr[0], 17);
//						eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR, READ_OP, &temp_arr[0], 17);
//						strncpy((char *)&http_user_info[total_reg_client + 1].passwrd_info[0], (char *)&temp_arr[1], REGISTRATION_KEY_LEN);
//						http_user_info[total_reg_client + 1].passwrd_info[REGISTRATION_KEY_LEN] = '\0';		
//						strncpy((char *)&http_user_info[total_reg_client + 1].user_info[0], (char *)&temp_arr[REGISTRATION_KEY_LEN + 1], USER_NAME_LEN);
//						http_user_info[total_reg_client + 1].user_info[USER_NAME_LEN] = '\0';		
//						total_reg_client++;
//					}
//					else{
//						status = '1';
//					}							
//				}
//				else
//					status = '1';	
//			}
//			else
//				status = '1';	
//		break;

//		case DEVICE_REGISTRATION:
//			temp += 2;	
//			if(system_info.status == 'Y'){
//			//	dflash_read_multiple_byte(ADMIN_PASSWRD_INFO_ADDR, &temp_arr[0], ADMIN_PASSWRD_LEN);
//			    eeprom_data_read_write(ADMIN_PASSWRD_INFO_ADDR, READ_OP, &temp_arr[0], ADMIN_PASSWRD_LEN);
//				if(strncmp((char *)&wms_payload_info->data[temp],(char *)&temp_arr[0], ADMIN_PASSWRD_LEN) == 0)
//					temp_flag = 1;
//				
////				for(temp = 0; temp < TOTAL_ADMIN_USER ; temp++){	
////					dflash_read_multiple_byte(ADMIN_USER_START_ADDR + temp*ADMIN_OFFSET , &read_arr[0], ADMIN_OFFSET);				
////					if(read_arr[1] == '1'){
////						if((strncmp((char *)&hc_payload->data[1], (char *)&read_arr[12], ADMIN_USER_PASSWRD_LEN)) == 0){
////							temp_flag = 1;
////							break;		
////						}
////					}	
////		   		}
//				temp += (ADMIN_PASSWRD_LEN + 2);
//				for(countr = 0; countr < total_client_license; countr++){
//				//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, temp_arr, DEVICE_REG_LEN);	  // status +mac id	
//					eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, READ_OP, temp_arr, DEVICE_REG_LEN);
//					if(temp_arr[0] == '1'){
//					//	loc_temp = MAC_ID_LEN;
//						if(strncmp((uint8_t*)&temp_arr[1], (uint8_t*)&wms_payload_info->data[temp], APP_ID_LEN) == 0){
//							temp_flag = 0;		// this device is already registered
//							break;				// break from function
//						}
//									
//					}
//				}
//				if(temp_flag == 1){
//					for(countr = 0; countr < total_client_license; countr++){
//					//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, &loc, 1);	  // status +mac id
//						eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, READ_OP, &loc, 1);	
//						if(loc == '0'){
//							//loc = 7;
//							write_arr[0] = '1';
//							memcpy(&write_arr[1], (char *)&wms_payload_info->data[temp],APP_ID_LEN);
//							temp += (APP_ID_LEN + 1);
//					
//							total_length = extract_json_field_val(&write_arr[1 + APP_ID_LEN], &wms_payload_info->data[temp]);   // name  
//							write_arr[1 + APP_ID_LEN + total_length] = '\0';
//							temp += 	(total_length + 2);	
//							count += total_length;
//							

//					//		memcpy(&write_arr[1 + APP_ID_LEN], (char *)&wms_payload_info->data[temp],	NAME_LEN - 1);
//					//		write_arr[1 + APP_ID_LEN + NAME_LEN - 1] = '\0';
//					//		temp += (NAME_LEN + 2);						
//							memcpy(&write_arr[1 + APP_ID_LEN+NAME_LEN], (char *)&wms_payload_info->data[temp],10);		// phone number	
//							write_arr[1 + APP_ID_LEN + NAME_LEN +10] = '\0';									
//						//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, write_arr, DEVICE_REG_LEN);
//						    eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + countr*DEVICE_REG_LEN, WRITE_OP, write_arr, DEVICE_REG_LEN);	 
//							status = 0x30;	
//							break;	 
//						}
//					}
//				}
//			}



//			
//		
////			if(system_info.status == 'Y'){
////				temp += 2;			/* skip " */
////				dflash_read_multiple_byte(REG_KEY_INFO_ADDR , &temp_arr[0], REGISTRATION_KEY_LEN*license_class);
////				for(count = 0; count < license_class*REGISTRATION_KEY_LEN ; count+= 4){						  // scan the registration key
////					if(strncmp((char *)&wms_payload_info->data[temp], (char *)&temp_arr[count], REGISTRATION_KEY_LEN) == 0){		 // if registration key	found in data base
////						key_match = 1;
////						break;	
////					}			
////				}
////				if(key_match == 0)
////					status =  INVALID_REGISTRATION_KEY;
////				else{
////					key_match = 0;
////					for(count = 0; count < license_class ; count++){
////						dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &temp_arr[0], DEVICE_REG_LEN);
////						if(strncmp((char *)&wms_payload_info->data[temp], (char *)&temp_arr[1], REGISTRATION_KEY_LEN) == 0){		 // if registration key	found in data base
////							if(temp_arr[0] == '1'){
////								loc_flag = 1;;
////								break;	
////							}
////							else if(temp_arr[0] == '0'){
////								pos = count;
////							}
////						}
////
//////						if(temp_arr[0] == '1'){
//////							if(strncmp((char *)&wms_payload_info->data[temp], (char *)&temp_arr[1], REGISTRATION_KEY_LEN) == 0){		 // if registration key	found in data base
//////								loc_flag = 1;;
//////								break;	
//////							}
//////						}
//////						if(temp_arr[0] == '0'){
//////							pos = count;
//////						}
////					}	
////					if(loc_flag == 0 && pos != 0xff){
////						count = 0;
////						write_arr[count++] = '1';			/* status */
////	
////						memcpy(&write_arr[count], (char *)&wms_payload_info->data[4], REGISTRATION_KEY_LEN);  /* registration key */
////						count += REGISTRATION_KEY_LEN;
////						temp += 5;
////						
////						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   /* read device mac id  */
////						temp += 	(total_length + 2);	
////						count += total_length;
////						mac_id_len = total_length;
////						
////						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   /* name  */
////						temp += 	(total_length + 2);	
////						count += total_length;
////						write_arr[count++] = '\0';
////	
////						temp += (NAME_LEN - total_length + 1);
////					//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + 1 + pos*DEVICE_REG_LEN, &write_arr[0], 1 + REGISTRATION_KEY_LEN + mac_id_len + count); // write client info 										
////	
////					//	count = 0;
////						total_length = extract_json_field_val(&write_arr[count], &wms_payload_info->data[temp]);   /* phone number */
////						temp += 	(total_length + 2);	
////						count += total_length;
////						write_arr[count++] = '\0';
////						dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + pos*DEVICE_REG_LEN, &write_arr[0], count); // write client info 										
////
////						dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + pos*DEVICE_REG_LEN, &temp_arr[0], 17);
////						strncpy((char *)&http_user_info[total_reg_client + 1].passwrd_info[0], (char *)&temp_arr[1], REGISTRATION_KEY_LEN);
////						http_user_info[total_reg_client + 1].passwrd_info[REGISTRATION_KEY_LEN] = '\0';		
////						strncpy((char *)&http_user_info[total_reg_client + 1].user_info[0], (char *)&temp_arr[REGISTRATION_KEY_LEN + 1], USER_NAME_LEN);
////						http_user_info[total_reg_client + 1].user_info[USER_NAME_LEN] = '\0';		
////						total_reg_client++;
////
////						status = REGISTRATION_SUCESSFULL;
////
////
////					}
////					else
////						status = INVALID_REGISTRATION_KEY;
////				}		
////			}
//			else
//				status = INVALID_REGISTRATION_KEY;	
//		break;

//	}	
//	return status;
//}


/************************************************************************//**
*	uint8_t authenticate_MAC_ID(struct gui_payload *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine authenticate the MAC id of tablet.
*				
*
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
uint8_t authenticate_MAC_ID(struct json_struct *wms_payload_info, uint8_t num_bytes){
	uint8_t count, read_arr[50], mac_id_match = 0x31;
	
	eeprom_data_read_write(SYSTEM_INFO_ADDR, READ_OP ,(uint8_t *)&system_info.status, 1);
	
	if(system_info.status == 'N'){
		mac_id_match = 0x32;	
	}
	else if(system_info.status == '3')	
		mac_id_match = '3';		
	else{
		for(count = 0; count < MAX_REG_KEY ; count++){						  // scan the registration key
		//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &read_arr[0], DEVICE_REG_LEN);
			eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, READ_OP, &read_arr[0], DEVICE_REG_LEN);
			if(read_arr[0] == '1'){
				if((strncmp((char *)&wms_payload_info->data[1], (char *)&read_arr[1], APP_ID_LEN)) == 0){		 // if registration key	found in data base
					mac_id_match = 0x30;
					glbl_arr[0] = mac_id_match;
					if(glbl_arr[0] == '0'){		// mac id match
						memcpy(&glbl_arr[1], &read_arr[1], 4);	
					}
					break;	
				}			
			}
		}	
	}
	return mac_id_match;

}


/************************************************************************//**
*	uint8_t fetch_device_registration_list(struct gui_payload *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine fetches the registered device list.
*				
*
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
void fetch_device_registration_list(void){
	uint8_t  read_arr[100], count, field_name_indx = 1, len;
	uint8_t no_of_GUI_clients_registered =0, temp;
	
	
	for(temp = 0; temp < total_client_license; temp++)
	{
		eeprom_data_read_write((DEVICE_REG_START_INFO_ADDR + (temp * DEVICE_REG_LEN)),READ_OP ,read_arr, DEVICE_REG_LEN);	  // status +mac id	
		if(read_arr[0] == '1')
		{
			no_of_GUI_clients_registered++;									
		}
	}

	ADD_START_BRACE;
	for(count = 0; count < total_client_license ; count++){						  // scan the registration key
	//dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &read_arr[0], DEVICE_REG_LEN);
		
	  eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , READ_OP, &read_arr[0], DEVICE_REG_LEN);
		
		if(read_arr[0] == '1')
		{
		add_field_name(field_name_indx++);
		ADD_DOUBLE_QUOTE;
		len = strlen((char *)&read_arr[1]);
		if(len > 0){
		//	strncpy((char *)json_send_ptr, (char *)&read_arr[1], 4);		// add registration key
		//	json_send_ptr += 4;
			*json_send_ptr++ = '0';
		}	
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;	

		add_field_name(field_name_indx++);
		ADD_DOUBLE_QUOTE;
		len = strlen((char *)&read_arr[17]);
		if(len > 0){
			strncpy((char *)json_send_ptr, (char *)&read_arr[17], BUYERS_NAME_LEN);		// add name
			json_send_ptr += BUYERS_NAME_LEN;
		}
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	 }
	} //end of for
	json_send_ptr--;
	ADD_CLOSING_BRACE;	
}


/************************************************************************//**
*	uint8_t remove_MAC_ID(struct gui_payload *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine remove the mac id from database.
*				
*
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
uint8_t remove_MAC_ID(struct json_struct *wms_payload_info, uint8_t num_bytes){
	uint8_t count, temp_arr[DEVICE_REG_LEN],  status = '1', len1 = 0 , len2 = 0;

	for(count = 0; count < total_client_license ; count++){
	//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &temp_arr[0], DEVICE_REG_LEN);
		eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, READ_OP, &temp_arr[0], DEVICE_REG_LEN);
		if(temp_arr[0] == '1'){
		//	len1 = strlen((char *)&temp_arr[17]); 
			if(strncmp((char *)&wms_payload_info->data[1], (char *)&temp_arr[17], BUYERS_NAME_LEN) == 0){		 // if registration key	found in data base
				status = '0';
			//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &status, 1);
				temp_arr[0] = '\0';
				eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, WRITE_OP, &temp_arr[0], 1);	
//			//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 17, &temp_arr[0], 1);
//			//	dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 27, &temp_arr[0], 1);
//				eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 17, WRITE_OP, &temp_arr[0], 1);
//				eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN + 27, WRITE_OP, &temp_arr[0], 1);
				break;	
			}
		}
	}	
	return status;

}
/************************************************************************//**
*	uint8_t device_replace(struct gui_payload *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine replace the device from database.
*				
*
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
//#ifdef 0
//
//uint8_t device_replace(struct gui_payload *wms_payload_info, uint8_t num_bytes){
//	uint8_t count, read_arr[30],  status = 0x30, reg_flag = 0, ret_flag = 0x0, loc;
//
//	for(loc = 0; loc < TOTAL_REG_KEY ; loc++){						  // scan the registration key
//		dflash_read_multiple_byte(DEVICE_REG_START_ADDR + loc*DEVICE_REG_LEN , &read_arr[0], DEVICE_REG_LEN);
//		if(read_arr[0] == '1'){
//			if((strncmp((char *)&wms_payload_info->data[6], (char *)&read_arr[7], MAC_ID_LEN)) == 0){		 // if registration key	found in data base
//				reg_flag = 1;
//				break;	
//			}
//		}
//	}
//	if(reg_flag == 1){
//		for(count = 0; count < TOTAL_REG_KEY ; count++){						  // scan the registration key
//			dflash_read_multiple_byte(DEVICE_REG_START_ADDR + count*DEVICE_REG_LEN , &read_arr[0], DEVICE_REG_LEN);
//			if((strncmp((char *)&wms_payload_info->data[0], (char *)&read_arr[1], REGISTRATION_KEY_LEN)) == 0){		 // if registration key	found in data base
//				status = 0x31;
//				dflash_write_multiple_byte(DEVICE_REG_START_ADDR + count*DEVICE_REG_LEN, &status,1);
//				dflash_write_multiple_byte(DEVICE_REG_START_ADDR + 7 + count*DEVICE_REG_LEN, &wms_payload_info->data[6], MAC_ID_LEN);	   
//				status = 0x30;
//				dflash_write_multiple_byte(DEVICE_REG_START_ADDR + loc*DEVICE_REG_LEN, &status,1);
//				ret_flag = MAC_ID_FOUND_REPLACED;
//				break;	
//			}
//		}	
//	}
//	else
//		ret_flag = MAC_ID_NOT_FOUND;
//	if(ret_flag == 0)
//		ret_flag = INVALID_REG_KEY;		
//	
//	return ret_flag;	
//	
//}
//#endif

/************************************************************************//**
*	void device_registration_init(){
*
* @brief			This routine is initializing the registration key .
*				
*
* @param 			none
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/04/13
* @note				None.                                            
****************************************************************************/
void extract_device_reg_info(void){
	uint8_t count,read_arr[20];

	for(count = 0; count < TOTAL_HTTP_SESSION; count++){		/* initialize */
		http_user_info[count].user_info[0]		= '\0';
		http_user_info[count].passwrd_info[0]	= '\0';
	}
	if(system_info.status == 'Y'){
		for(count = 0; count < TOTAL_HTTP_SESSION; count++){
		//	dflash_read_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, &read_arr[0], 17);
			eeprom_data_read_write(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN, READ_OP, &read_arr[0], 17);

			if(read_arr[0] == '1'){		/* if it is registered */
				strncpy((char *)&http_user_info[total_reg_client + 1].passwrd_info[0], (char *)&read_arr[1], REGISTRATION_KEY_LEN);
				http_user_info[total_reg_client + 1].passwrd_info[REGISTRATION_KEY_LEN] = '\0';		
				strncpy((char *)&http_user_info[total_reg_client + 1].user_info[0], (char *)&read_arr[REGISTRATION_KEY_LEN + 1], USER_NAME_LEN);
				http_user_info[total_reg_client + 1].user_info[USER_NAME_LEN] = '\0';		
				total_reg_client++;
			}	
		}
	}	
}


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


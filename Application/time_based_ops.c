#include <stdlib.h>
#include "stm32f0xx.h"
#include "stm32f0xx_tim.h"
#include "memory_info.h"
#include "port_mapping.h"
#include "eeprom.h"
#include "pin_config.h"
#include "io_system.h"
#include "system.h"

#define YES		1
#define	NO		0

extern struct system_params sys_info;
extern struct led_params led_info;
extern struct input_port_mapping inps[MAX_INPUT_PORTS];

/********************************************************************************************************************************
 * Function name: 	void serve_sampling_window_timer (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine serves as the ISR for 1 Second Timer.
 *
 * Notes:			Called every second, increments sampling window counter.
 *					Selects Activity level at the end of every 15s sampling window.
 *					Updates ON duration if activities are sensed.
 *******************************************************************************************************************************/
void serve_sampling_window_timer (void) {
	uint8_t idx;
	/* If 1 second interrupt sensed. */
	if (sys_info.__1sFlag) {
		sys_info.__1sFlag = 0;
		sys_info.sampling_window++;

//		toggle_sys_led ();

		for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
			if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
				if (inps [idx].pir_info->on_duration) {
					inps [idx].pir_info->on_duration--;
					if (!inps [idx].pir_info->on_duration) {
						execute_op_ops (inps [idx].port_pool, PORT_STATE_OFF);
					}
					if (inps [idx].curr_state == LOGIC_LEVEL_LOW) {
						if (inps [idx].pir_info->sense_window++ >= 3) {
							inps [idx].pir_info->sense_window	= 0;
							inps [idx].pir_info->sensor_count++;
						}
					}
					else {
						inps [idx].pir_info->sense_window	= 0;
					}
				}
			}
		}

		chk_if_pulse_to_be_sent ();

		if (sys_info.sys_mode == MODE_AUTOMATIC) {
		/* Proceed only if system is in Automatic Mode. */
			if (sys_info.sampling_window == 15) {
				for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
					if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
						if (inps [idx].pir_info->sensor_count == 0) {
							inps [idx].pir_info->activity_level = OCCUPANCY_NULL;
							if (inps [idx].pir_info->on_duration) {
								if (++inps [idx].pir_info->null_window_counter >= inps [idx].pir_info->null_cnt_windows) {
									inps [idx].pir_info->on_duration -= (inps [idx].pir_info->on_duration * inps [idx].pir_info->decrease_percentage)/100;
									inps [idx].pir_info->null_window_counter = 0;
								}
							}
							else {
									inps [idx].pir_info->null_window_counter = 0;
							}
						}else
						if (inps [idx].pir_info->sensor_count == 1) {
							inps [idx].pir_info->activity_level = OCCUPANCY_LOW;
							inps [idx].pir_info->null_window_counter = 0;
							inps [idx].pir_info->on_duration += MINS(inps [idx].pir_info->low_activity_config_time);
						}else
						if (inps [idx].pir_info->sensor_count == 2) {
							inps [idx].pir_info->activity_level = OCCUPANCY_MEDIUM;
							inps [idx].pir_info->null_window_counter = 0;
							inps [idx].pir_info->on_duration += MINS(inps [idx].pir_info->medium_activity_config_time);
						}else
						if (inps [idx].pir_info->sensor_count >= 3) {
							inps [idx].pir_info->activity_level = OCCUPANCY_HIGH;
							inps [idx].pir_info->null_window_counter = 0;
							inps [idx].pir_info->on_duration += MINS(inps [idx].pir_info->high_activity_config_time);
						}
						inps [idx].pir_info->sensor_count = 0;
					}
					sys_info.sampling_window = 1;
				}
			}
		}
	}
}

/********************************************************************************************************************************
 * Function name: 	uint8_t validate_sig_as (uint8_t idx, uint8_t level)
 *
 * Returns: 		uint8_t			returns YES (1), if signal is validated as level passed in argument 2. Otherwise NO (0) is returned.
 *
 * Arguments: 		uint8_t idx		Input Number.
 *					uint8_t level	LOGIC_LEVEL_HIGH / LOGIC_LEVEL_LOW
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine serves as the ISR for 25ms Timer.
 *
 * Notes:			Called when an input is triggered by rising or falling edge.
 *					Determines if stable high or low level is sensed.
 *******************************************************************************************************************************/
void handle_ip_trig_params (uint8_t idx) {
	inps [idx].input_debounce_counter = 0;
	sys_info.check_di_debounce &= ~(1<<idx);
//	pin_int_ctrl (idx, PORT_STATE_ON);
}

/********************************************************************************************************************************
 * Function name: 	uint8_t validate_sig_as (uint8_t idx, uint8_t level)
 *
 * Returns: 		uint8_t			returns YES / NO
 *
 * Arguments: 		uint8_t idx		Input Number.
 *					uint8_t level	LOGIC_LEVEL_HIGH / LOGIC_LEVEL_LOW
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to validate the sensed logic level as passed in argument 2.
 *
 * Notes:			YES = 1
 *					No  = 0
 *******************************************************************************************************************************/
uint8_t validate_sig_as (uint8_t idx, uint8_t level) {
	uint8_t ret = NO;
	if (read_input_pin (idx) == level) {
		if (inps [idx].input_debounce_counter >= 8) {
			inps [idx].valid_input_sensed = 1;

			if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
				if (inps [idx].curr_state == LOGIC_LEVEL_LOW) {
					inps [idx].pir_info->sensor_count++;
				}
			}
			handle_ip_trig_params (idx);
			ret = YES;
		}
	}
	else {
		handle_ip_trig_params (idx);
	}
	return ret;					
}

/********************************************************************************************************************************
 * Function name: 	void serve_debounce_check_timer (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine serves as the ISR for 25ms Timer.
 *
 * Notes:			Called when an input is triggered by rising or falling edge.
 *					Determines if stable high or low level is sensed.
 *******************************************************************************************************************************/
void serve_debounce_check_timer (void) {
	uint8_t idx;
	if (sys_info.__25msFlag) {
		for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
			if (sys_info.check_di_debounce & (1<<idx)) {
				if (++inps [idx].input_debounce_counter >= 4) {
					if (inps [idx].curr_state) {
						validate_sig_as (idx, LOGIC_LEVEL_HIGH);
					}
					else {
						validate_sig_as (idx, LOGIC_LEVEL_LOW);	
					}
				}
			}
		}
		if (!sys_info.check_di_debounce) {
			TIM_Cmd(TIM2, DISABLE);			  
		}
		sys_info.__25msFlag = 0;
	}  
}

void serve_led_timer (void) {
	if (led_info.__100msFlag) {
		if (led_info.sys_led_100ms_iter < led_info.sys_led_timeout) {
			SYS_LED_ON;
		}else {
			SYS_LED_OFF;
		}
		led_info.sys_led_100ms_iter++;
//		if (led_info.comm_led_100ms_iter < led_info.comm_led_timeout) {
//			COMM_LED_ON;
//		}else {
//			COMM_LED_OFF;
//		}
		if (led_info.comm_led_timeout) {
			COMM_LED_ON;
			led_info.comm_led_timeout--;
		}else {
			COMM_LED_OFF;
		}
		led_info.__100msFlag = 0;
	}
}


//===========================================================================================================================
/*
	Func. Name : 	void toggle_comm_led(uint16_t times)

	Description : 	This func. toggles the communication led a specified no. of times .

  @arg : uint16_t times

	@return : none
	@author : Parvesh
	@Date: 23/06/2015
*/
//===========================================================================================================================

void toggle_comm_led(uint16_t times)
{
	uint16_t i;
	
	for(i=1;i<=times;i++)
	{
	  COMM_LED_ON;
		Delay(500000);
		COMM_LED_OFF;
		Delay(500000);
	}
}


//===========================================================================================================================
/*
	Func. Name : 	void Delay(uint32_t del)

	Description : 	This func. provides delay.

  @arg : uint32_t del

	@return : none
	@author : Parvesh
	@Date: 23/06/2015
*/
//===========================================================================================================================
void Delay(uint32_t del)
{
		while(del--);
}

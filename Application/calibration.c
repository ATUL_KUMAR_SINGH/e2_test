/************************************************************************//**
* @file			calibration.c
*
* @brief		Contains  the inialization of all the peripherals
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 02/06/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include<stm32f0xx.h>
//#include <RTL.h>
//#include "misc.h"
#include "uart.h"
//#include "ethernet_packet.h"
#include "activateModules.h"
#include "automated_tasks.h"
#include "client_registration.h"
#include "target.h"
#include "water_management.h"
#include "buzzer.h"
#include "bitops.h"
#include "sensor.h"
#include "eeprom.h"
//#include "spi.h"
//#include "virtual_sensor.h"


/*
**===========================================================================
**		Defines section
**===========================================================================
*/
extern struct wms_packet gui_send;
//extern struct tank_info OHT_tank, UGT_tank;
extern struct wms_sys_info wms_sys_config;
uint32_t oht_inflow_rate, ugt_inflow_rate;
uint8_t oht_flow_rate_present, ugt_flow_rate_present, oht_tank_calibrated, ugt_tank_calibrated;
uint32_t oht_time_bw_sensor[10],ugt_time_bw_sensor[8], oht_time_bw_virtual_sensor[21], ugt_time_bw_virtual_sensor[21];
uint32_t oht_total_filled_time, ugt_total_filled_time;
extern struct real_sensor_info  oht_real_sensor_config[TOTAL_OHT_TANK], ugt_real_sensor_config;
extern struct virtual_sensor_info oht_virtual_sensor_config, ugt_virtual_sensor_config;
extern struct tank_setting oht_tank_config[TOTAL_OHT_TANK], ugt_tank_config;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif


/*
**===========================================================================
**		Function description
**===========================================================================
*/

/************************************************************************//**
*	void save_inflow_rate(struct gui_payload *wms_payload_info, uint8_t num_bytes)
*
* @brief			This routine saves the inflow rate of tank .
*				
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				01/06/14
* @note				None.                                            
****************************************************************************/
void save_inflow_rate(struct json_struct *wms_payload_info, uint8_t num_bytes){
//	uint8_t temp = 0, tank_num, count, status = 1, read_arr[50];
////	float total_filled_time;
//
//	do{
//		tank_num = ascii_decimal(&wms_payload_info->data[temp], 2);
//		temp += 2;
//		if(tank_num <= 25){
//			dflash_write_multiple_byte (SAVE_OHT_FLOW_RATE_SETTING_ADDR, (uint8_t*)&wms_payload_info->data[temp], 36);
//			dflash_read_multiple_byte (SAVE_OHT_FLOW_RATE_SETTING_ADDR, &read_arr[0], 36);
//			oht_inflow_rate = ascii_decimal(&wms_payload_info->data[temp], 6);
//			oht_inflow_rate /= 100;
//		//	oht_inflow_rate =  (oht_inflow_rate* 1000)/60;			  // in litre per minute
//			oht_total_filled_time = (OHT_tank.tank_config_ptr->total_volume *1000)/ oht_inflow_rate;	  // in litre per second
//			dflash_write_multiple_byte(OHT_FLOW_RATE_SAVED_FLAG, &status, 1);
//		  	dflash_read_multiple_byte(OHT_FLOW_RATE_SAVED_FLAG, &oht_flow_rate_present, 1);
//			for(count = 0; count < OHT_tank.real_sensor_ptr->total_sensor; count++ ){
//				oht_time_bw_sensor[count] = (oht_total_filled_time *OHT_tank.real_sensor_ptr->cap_bw_sensor_perc[count])/ (100 * 60);	
//			}
//		//	oht_tank_calibrated = 1;
//		//	dflash_write_multiple_byte(OHT_VOLUME_SAVED_FLAG, &oht_tank_calibrated, 1);
//		}
//		else{
//			dflash_write_multiple_byte (SAVE_UGT_FLOW_RATE_SETTING_ADDR, (uint8_t*)&wms_payload_info->data[temp], 36);
//			dflash_read_multiple_byte (SAVE_UGT_FLOW_RATE_SETTING_ADDR, &read_arr[0], 36);
//			ugt_inflow_rate = ascii_decimal(&wms_payload_info->data[temp], 6);
//			ugt_inflow_rate /= 100;
//			ugt_total_filled_time = (UGT_tank.tank_config_ptr->total_volume * 1000) /ugt_inflow_rate;
//			dflash_write_multiple_byte(UGT_FLOW_RATE_SAVED_FLAG, &status, 1);
//			dflash_read_multiple_byte(UGT_FLOW_RATE_SAVED_FLAG, &ugt_flow_rate_present, 1);
//			for(count = 0; count < UGT_tank.real_sensor_ptr->total_sensor; count++ ){
//				ugt_time_bw_sensor[count] = (ugt_total_filled_time * UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[count])/(100 * 60);	
//			}
//			//ugt_tank_calibrated = 1;
//		//	dflash_write_multiple_byte(UGT_VOLUME_SAVED_FLAG, &ugt_tank_calibrated, 1);
//			//dflash_read_multiple_byte (SAVE_OHT_FLOW_RATE_SETTING_ADDR, &read_arr[0], 21);
//		}
//		temp += 36;
//
//	}while(num_bytes != temp);
//
//	#ifdef VIRTUAL_SENSOR
//		calibrate_oht_virtual_system(1);
//		if(wms_sys_config.total_tank > 1)
//			calibrate_ugt_virtual_system(1);
//	#endif
}

/************************************************************************//**
*	void save_tank_info(struct gui_payload *wms_payload_info, uint8_t num_bytes)
*
* @brief			This routine saves the tank information.
*				
* @param 			struct gui_payload *payload_info - pointer to structure in which data to be filled.
* @param 			num_bytes		     					- number of bytes to be transfered.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				01/06/14
* @note				None.                                            
****************************************************************************/
//uint8_t save_tank_info(struct gui_payload *wms_payload_info, uint8_t num_bytes){
//   	uint8_t temp = 0, tank_num,  status = 1, read_arr[30];  //, *temp_ptr;
////	uint32_t tank_volume, len, radius, width, height;
//
////	temp_ptr = (uint8_t *)&tank_volume;
//	do{
//		tank_num = ascii_decimal(&wms_payload_info->data[temp], 2);
//		temp += 2;
//		if(tank_num <= 25){
//			OHT_tank.tank_config_ptr->tank_type = ascii_decimal(&wms_payload_info->data[temp++], 1);
//			OHT_tank.tank_config_ptr->tank_num = tank_num;
//			switch(OHT_tank.tank_config_ptr->tank_type){
//				case CUSTOM_TYPE:
//					OHT_tank.tank_config_ptr->total_volume = ascii_decimal(&wms_payload_info->data[temp], 6);
//					//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//					temp+= 6;
//				break;
//
//				case CUBICAL_TYPE:							/* save length , width , height */
//				//	dflash_w57rite_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, (uint8_t *)&wms_payload_info->data[2], 19);
//					OHT_tank.tank_config_ptr->param1 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					OHT_tank.tank_config_ptr->param1 = OHT_tank.tank_config_ptr->param1/100;
//					temp += 6;
//					OHT_tank.tank_config_ptr->param2 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					OHT_tank.tank_config_ptr->param2  = OHT_tank.tank_config_ptr->param2 /100;
//					temp += 6;
//					OHT_tank.tank_config_ptr->param3  = ascii_decimal(&wms_payload_info->data[temp], 6);
//					OHT_tank.tank_config_ptr->param3 = OHT_tank.tank_config_ptr->param3/100;
//					temp += 6;
//					OHT_tank.tank_config_ptr->total_volume = (OHT_tank.tank_config_ptr->param1*OHT_tank.tank_config_ptr->param2*OHT_tank.tank_config_ptr->param3) * 1000;
//				//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//				break;
//
//				case CYLINDRICAL_TYPE:		   /* save length , radius */
//				//	dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, (uint8_t *)&wms_payload_info->data[2], 13);	
//					OHT_tank.tank_config_ptr->param1 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					OHT_tank.tank_config_ptr->param1 = OHT_tank.tank_config_ptr->param1/100;
//					temp += 6;
//					OHT_tank.tank_config_ptr->param2 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					OHT_tank.tank_config_ptr->param2 = OHT_tank.tank_config_ptr->param2 /100;
//					temp += 6;
//					OHT_tank.tank_config_ptr->total_volume = 3.142857 *OHT_tank.tank_config_ptr->param2 *OHT_tank.tank_config_ptr->param2*OHT_tank.tank_config_ptr->param1 * 1000; 
//				//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//				break;
//
//			}
//			dflash_write_multiple_byte(OHT_VOLUME_SAVED_FLAG, &status, 1);
//			dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, (uint8_t *)&OHT_tank.tank_config_ptr->tank_num, TANK_SETTING_LEN);
////			for(loop = 0; loop < TANK_SETTING_LEN; loop++)
////				loc_arr[loop] = (uint8_t *)&OHT_tank.tank_config_ptr->tank_num	
////			memcpy(loc_arr, (uint8_t *)&OHT_tank.tank_config_ptr->tank_num, TANK_SETTING_LEN);
////			dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR + 6, (uint8_t *)&OHT_tank.tank_config_ptr->param1, 4);
////			dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR + 6 + 4, (uint8_t *)&OHT_tank.tank_config_ptr->param2, 4);
////			dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR + 6 + 4 +4, (uint8_t *)&OHT_tank.tank_config_ptr->param3, 4);
////			dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, loc_arr, TANK_SETTING_LEN);
//			dflash_read_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, &read_arr[0], TANK_SETTING_LEN);						
//			dflash_read_multiple_byte(OHT_VOLUME_SAVED_FLAG, &oht_tank_calibrated, 1);
//		}
//		else if(tank_num > 25 && tank_num <= 50){
//			wms_sys_config.total_tank = 2;
//			dflash_write_multiple_byte (TOTAL_TANKS, (uint8_t *)&wms_sys_config.total_tank, 1);
//			UGT_tank.tank_config_ptr->tank_type = ascii_decimal(&wms_payload_info->data[temp++], 1);
//			UGT_tank.tank_config_ptr->tank_num = tank_num;
//			switch(UGT_tank.tank_config_ptr->tank_type){
//				case CUSTOM_TYPE:
//					UGT_tank.tank_config_ptr->total_volume = ascii_decimal(&wms_payload_info->data[temp], 6);
//					//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//					temp+= 6;
//				break;
//
//				case CUBICAL_TYPE:							/* save length , width , height */
//				//	dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, (uint8_t *)&wms_payload_info->data[2], 19);
//					UGT_tank.tank_config_ptr->param1 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					UGT_tank.tank_config_ptr->param1 = UGT_tank.tank_config_ptr->param1/100;
//					temp += 6;
//					UGT_tank.tank_config_ptr->param2 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					UGT_tank.tank_config_ptr->param2  = UGT_tank.tank_config_ptr->param2 /100;
//					temp += 6;
//					UGT_tank.tank_config_ptr->param3  = ascii_decimal(&wms_payload_info->data[temp], 6);
//					UGT_tank.tank_config_ptr->param3 = UGT_tank.tank_config_ptr->param3/100;
//					temp += 6;
//					UGT_tank.tank_config_ptr->total_volume = (UGT_tank.tank_config_ptr->param1*UGT_tank.tank_config_ptr->param2*UGT_tank.tank_config_ptr->param3 * 1000);
//				//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//				break;
//
//				case CYLINDRICAL_TYPE:		   /* save length , radius */
//				//	dflash_write_multiple_byte(SAVE_OHT_TANK_SETTING_ADDR, (uint8_t *)&wms_payload_info->data[2], 13);	
//					UGT_tank.tank_config_ptr->param1 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					UGT_tank.tank_config_ptr->param1 = UGT_tank.tank_config_ptr->param1/100;
//					temp += 6;
//					UGT_tank.tank_config_ptr->param2 = ascii_decimal(&wms_payload_info->data[temp], 6);
//					UGT_tank.tank_config_ptr->param2 = UGT_tank.tank_config_ptr->param2 /100;
//					temp += 6;
//					UGT_tank.tank_config_ptr->total_volume = 3.142857 *UGT_tank.tank_config_ptr->param2 *UGT_tank.tank_config_ptr->param2*UGT_tank.tank_config_ptr->param1 * 1000; 
//				//	OHT_tank.tank_config_ptr->total_volume = tank_volume;
//				break;
//
//			}
//			dflash_write_multiple_byte(UGT_VOLUME_SAVED_FLAG, &status, 1);
//			dflash_write_multiple_byte(SAVE_UGT_TANK_SETTING_ADDR, (uint8_t *)&UGT_tank.tank_config_ptr->tank_num, TANK_SETTING_LEN);
//			dflash_read_multiple_byte(UGT_VOLUME_SAVED_FLAG, &ugt_tank_calibrated, 1);
//
////			dflash_write_multiple_byte(UGT_VOLUME_SAVED_FLAG, &status, 1);
////			dflash_read_multiple_byte(UGT_VOLUME_SAVED_FLAG, &ugt_tank_calibrated, 1);
//		}
//	//	temp += 20;
//
//	}while(num_bytes != temp);
//	
//	#ifdef VIRTUAL_SENSOR
//		calibrate_oht_virtual_system(1);
//		if(wms_sys_config.total_tank > 1)
//			calibrate_ugt_virtual_system(1);
//	#endif
//	return 1;
//
//}

void tank_sensor_calibrate(void){
	uint8_t loc = 0, locl = 0, indx;

	wms_sys_config.total_ugt = 0;
	wms_sys_config.total_oht = 0;
	for(indx = 0; indx < TOTAL_OHT_TANK; indx++){
		if(OHT_tank[indx].tank_config_ptr->tank_state == 1){
			if(OHT_tank[indx].real_sensor_ptr->equidistant_flag == 0){											 /* if sensors are equidistant */
				for(loc = 0; loc < OHT_tank[indx].real_sensor_ptr->total_sensor ; loc++){
					OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[loc] =  100/OHT_tank[indx].real_sensor_ptr->total_sensor;	/* calculates the distance between sensor */
					locl += OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[loc];
				}
				//	locl = OHT_tank.real_sensor_ptr->total_sensor *wms_sys_config.oht_cap_bw_sensor_perc[0];
				locl = 100 - locl;
				if(locl > 0){
					OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[OHT_tank[indx].real_sensor_ptr->total_sensor - 1] += locl;	
				}
			}
			else if(OHT_tank[indx].real_sensor_ptr->equidistant_flag == 1){			/* if sensors are equidistant from 1 */
				OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[0] = 0;
				for(loc = 1; loc < OHT_tank[indx].real_sensor_ptr->total_sensor ; loc++){		   /* calculate the capacity between sensors */
					OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[loc] =  100/(OHT_tank[indx].real_sensor_ptr->total_sensor - 1);
					locl += OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[loc];
				}
				locl = 100 - locl;
				if(locl > 0){
					OHT_tank[indx].real_sensor_ptr->cap_bw_sensor_perc[OHT_tank[indx].real_sensor_ptr->total_sensor - 1] += locl;	
				}
			}
			wms_sys_config.total_oht++;
		}
	}
	locl = 0;
	if(UGT_tank.tank_config_ptr->tank_state == 1){
		if(UGT_tank.real_sensor_ptr->equidistant_flag == 0){											 /* if sensors are equidistant */
			for(loc = 0; loc < UGT_tank.real_sensor_ptr->total_sensor ; loc++){	  
				UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[loc] =  100/UGT_tank.real_sensor_ptr->total_sensor;	/* calculates the distance between sensor */
				locl += UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[loc];
			}
			//	locl = OHT_tank.real_sensor_ptr->total_sensor *wms_sys_config.oht_cap_bw_sensor_perc[0];
			locl = 100 - locl;
			if(locl > 0){
				UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[UGT_tank.real_sensor_ptr->total_sensor - 1] += locl;	
			}
		}
		else if(UGT_tank.real_sensor_ptr->equidistant_flag == 1){			/* if sensors are equidistant from 1 */
			UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[0] = 0;
			for(loc = 1; loc < UGT_tank.real_sensor_ptr->total_sensor ; loc++){		   /* calculate the capacity between sensors */
				UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[loc] =  100/(UGT_tank.real_sensor_ptr->total_sensor - 1);
				locl += UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[loc];
			}
			locl = 100 - locl;
			if(locl > 0){
				UGT_tank.real_sensor_ptr->cap_bw_sensor_perc[UGT_tank.real_sensor_ptr->total_sensor - 1] += locl;	
			}
		}
		wms_sys_config.total_ugt = 1;
		wms_sys_config.ugt_pump_select = UGT_tank.pump_select;
	}
	
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/








#include <stdlib.h>
#include "stm32f0xx.h"
#include "stm32f0xx_tim.h"
#include "memory_info.h"
#include "port_mapping.h"
#include "eeprom.h"
#include "pin_config.h"
#include "io_system.h"
#include "timer.h"
#include "system.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
__IO uint32_t VectorTable[48] __attribute__((at(0x20000000)));
struct system_params sys_info;
struct led_params led_info;
extern struct input_port_mapping inps[MAX_INPUT_PORTS];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/********************************************************************************************************************************
 * Function name: 	void pre_init (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to pre-init the system.
 *
 * Notes:			Providing startup delay & manually relocating the interrupt vector table.
 *******************************************************************************************************************************/
void pre_init (void) {
	volatile uint32_t loop;

	/* Manually relocate vector table to the internal SRAM at 0x20000000 */  
	/* Copy the vector table from the Flash & map it to the base address of the SRAM at 0x20000000. */
	for(loop = 0; loop < 48; loop++) {
		VectorTable[loop] = *(__IO uint32_t*)(APPLICATION_ADD + (loop<<2));
	}
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;   /* Enable the SYSCFG peripheral clock*/
	SYSCFG->CFGR1 = SYSCFG_CFGR1_MEM_MODE;  /* Remap SRAM at 0x00000000 */

	for (loop = 0; loop < 999999; loop++);
	for (loop = 0; loop < 999999; loop++);
	for (loop = 0; loop < 999999; loop++);
	for (loop = 0; loop < 999999; loop++);
}

/********************************************************************************************************************************
 * Function name: 	void init_led_params (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	8 Apr, 2015
 *
 * Description: 	This routine is used to initialise SYSTEM & COMM led patterns.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void init_led_params (void) {
	if (sys_info.slave_add != 0xFF) {
		led_info.sys_led_timeout = 10;	// 100ms.
	}else {
		led_info.sys_led_timeout = 2;	// 20ms.
	}
	
	led_info.comm_led_timeout = 0;
	timer_init(TIM6, TIME_MILLI_SEC, 100);
	TIM_Cmd(TIM6, ENABLE);
}

/********************************************************************************************************************************
 * Function name: 	void init_sys_params (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	Initializes all the System Paramters.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void init_sys_params (void) {
	uint8_t idx = 0, *uid_ptr = ((uint8_t*)UIDMSWORDADD), check_first_time_run_flag = 0;
	uint16_t random_seed = 0;

	sys_info.sampling_window				= 1;
	sys_info.check_di_debounce				= 0;
	sys_info.__1sFlag						= 0;
	sys_info.__25msFlag						= 0;

//	eeprom_data_read_write(PREV_FW_VER_ADD, WRITE_OP, (uint8_t *)&FWver, sizeof (uint32_t));
//	eeprom_data_read_write(CURR_FW_VER_ADD, WRITE_OP, (uint8_t *)&FWver, sizeof (uint32_t));

	eeprom_data_read_write(SELF_SLAVE_ADD, READ_OP, (uint8_t *)&sys_info.slave_add, 1);
//	sys_info.slave_add = 2;
	set_default ();
	eeprom_data_read_write(FIRST_RUN_FLAG_ADD, READ_OP, (uint8_t *)&check_first_time_run_flag, 1);
	if (check_first_time_run_flag != 0x55) {
		set_default ();
		check_first_time_run_flag = 0x55;
		eeprom_data_read_write(FIRST_RUN_FLAG_ADD, WRITE_OP, (uint8_t *)&check_first_time_run_flag, 1);
	}

	for (idx = 0; idx < 12; idx++) {
		random_seed += *(uid_ptr + idx);
	}
	srand(random_seed);
}

/********************************************************************************************************************************
 * Function name: 	void init_pir_input_port (struct input_port_mapping *src_input)
 *
 * Returns: 		None
 *
 * Arguments: 		struct input_port_mapping *src_input		pir type input for which a pir related parameters node has
 *																to be allocated from heap.
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to allocate pir related parameters node from heap & attach it to a pir type input.
 *
 * Notes:			None
 *******************************************************************************************************************************/
void init_pir_input_port (uint8_t ip_idx, struct input_port_mapping *src_input) {
	uint16_t read_from_eeprom_var;

	if (src_input->pir_info == NULL) {
		/* Creating a new pir_info nodeif previously not existing. */
		src_input->pir_info = (struct pir_params *)malloc (sizeof (struct pir_params));
	}
	clear_pir_input_port_parameters (src_input->pir_info);

	/* Reading Low Activity Time for Current input. */
	eeprom_data_read_write(PIR_LOW_ACTIVITY_INFO(ip_idx), READ_OP, (uint8_t *)&read_from_eeprom_var, sizeof (uint16_t));
	src_input -> pir_info -> low_activity_config_time = read_from_eeprom_var;

	/* Reading Medium Activity Time for Current input. */
	eeprom_data_read_write(PIR_MEDIUM_ACTIVITY_INFO(ip_idx), READ_OP, (uint8_t *)&read_from_eeprom_var, sizeof (uint16_t));
	src_input -> pir_info -> medium_activity_config_time = read_from_eeprom_var;

	/* Reading High Activity Time for Current input. */
	eeprom_data_read_write(PIR_HIGH_ACTIVITY_INFO(ip_idx), READ_OP, (uint8_t *)&read_from_eeprom_var, sizeof (uint16_t));
	src_input -> pir_info -> high_activity_config_time = read_from_eeprom_var;

	/* Reading Null Count Windows for Current input. */
	eeprom_data_read_write(PIR_NULL_CNT_WINDOW(ip_idx), READ_OP, (uint8_t *)&read_from_eeprom_var, sizeof (uint8_t));
	src_input -> pir_info -> null_cnt_windows = read_from_eeprom_var;

	/* Reading Decrease Percentage for Current input. */
	eeprom_data_read_write(PIR_DEC_PRCNT(ip_idx), READ_OP, (uint8_t *)&read_from_eeprom_var, sizeof (uint8_t));
	src_input -> pir_info -> decrease_percentage = read_from_eeprom_var;

	if (sys_info.sys_mode == MODE_AUTOMATIC) {
		/* ON Duration after which OFF operation has to be executed on all the associated output ports of current input. */
		src_input -> pir_info -> on_duration = src_input -> pir_info -> medium_activity_config_time * 60;
	}
}

void process_pir_activity_time_pkt (uint8_t *settings_data_ptr) {
	uint16_t activity;

	if (settings_data_ptr [0] < MAX_INPUT_PORTS) {
		/* Input Port Valid. */
		if (inps [settings_data_ptr [0]].input_port_type == INPUT_PORT_TYPE_PIR) {
			
			activity = settings_data_ptr [1];
			activity <<= 8;
			activity |= settings_data_ptr [2];
			/* Writing High Activity Time for Current input. */
			eeprom_data_read_write(PIR_HIGH_ACTIVITY_INFO(settings_data_ptr [0]), WRITE_OP, (uint8_t *)&activity, sizeof (uint16_t));
			inps [settings_data_ptr [0]]. pir_info -> high_activity_config_time = activity;

			activity = settings_data_ptr [3];
			activity <<= 8;
			activity |= settings_data_ptr [4];
			/* Writing Medium Activity Time for Current input. */
			eeprom_data_read_write(PIR_MEDIUM_ACTIVITY_INFO(settings_data_ptr [0]), WRITE_OP, (uint8_t *)&activity, sizeof (uint16_t));
			inps [settings_data_ptr [0]]. pir_info -> medium_activity_config_time = activity;
			
			activity = settings_data_ptr [5];
			activity <<= 8;
			activity |= settings_data_ptr [6];
			/* Writing Low Activity Time for Current input. */
			eeprom_data_read_write(PIR_LOW_ACTIVITY_INFO(settings_data_ptr [0]), WRITE_OP, (uint8_t *)&activity, sizeof (uint16_t));
			inps [settings_data_ptr [0]]. pir_info -> low_activity_config_time = activity;

		}
	}

}

/********************************************************************************************************************************
 * Function name: 	void init_pir_subsystem (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to initialize all the pir type inputs & there sub nodes. In addition to this,
 *					It enables the sampling window timer (Timer 1).
 *
 * Notes:			None
 *******************************************************************************************************************************/
void init_pir_subsystem (void) {
	uint8_t idx;
	/* Checking all the inputs. */
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* If the inputs are of PIR type, then create a new pir parameter structure node
			from heap & initialize all the members of created node. */
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_PIR) {
			init_pir_input_port (idx, &inps [idx]);
		}
	}
	/* Initialize & Enable the sampling window timer. */
	timer_init(TIM1, TIME_SEC, 1);
	TIM_Cmd(TIM1, ENABLE);
}

/********************************************************************************************************************************
 * Function name: 	void init_system_mode (void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	12 Feb, 2015
 *
 * Description: 	This routine is used to initialize system mode.
 *
 * Notes:			System mode can either be AUTOMATIC / MANUAL.
 *******************************************************************************************************************************/
void init_system_mode (void) {
	uint8_t idx;
	/* Checking all the inputs. */
	for (idx = 0; idx < MAX_INPUT_PORTS; idx++) {
		/* If the input is of MODE SELECT type, then select appropriate system mode. */
		if (inps [idx].input_port_type == INPUT_PORT_TYPE_MODE_SELECT) {
			inps [idx].curr_state = inps [idx].prev_state = read_input_pin (idx);
			if (inps [idx].curr_state == LOGIC_LEVEL_HIGH) {
				sys_info.sys_mode = MODE_AUTOMATIC;
			}else {
				sys_info.sys_mode = MODE_MANUAL;
			}
		}
	}
}

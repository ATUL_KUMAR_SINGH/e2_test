/************************************************************************//**
* @file			tcp.h
*
* @brief		Header for tcp.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 10/12/11 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef TCP_H
#define TCP_H

#include"json_client.h"

/*
**===========================================================================
**						--- macros ---
**===========================================================================
*/

#define 	ETH_RX_BUF_LEN 			2048//768			/* Receive Buffer for Temprory hold data receive */
#define 	ETH_TX_BUF_LEN 			528			/* Transmit Buffer for Temprory hold data receive */

#define USER_NUM_SESS		4
#define USER_STATE_IDLE		0
#define USER_STATE_ACTIVE	1
#define USER_STATE_WAITING	2
#define USER_STATE_RESERVED	3
#define USER_STATE_ERROR	4
#define USER_FLAG_FOPENED	5

#define TCP_TOS_NORMAL		0
#define TCP_DEF_TOUT		20 					//Timeout to kill connection after 10 hours if no transaction takes place at TCP/IP

#define USER_SERVER_PORT 	 2013

#define	TOTAL_HTTP_SESSION	4

#define	LINK_UP				1

#define	LINK_DOWN			2


/*
**===========================================================================
**		Defines section
**===========================================================================
*/
typedef struct{
	unsigned char 	Count;
	unsigned char 	Socket;
	unsigned char 	State;
	unsigned char 	Flags;
	unsigned char	type;			// type - 0 in case of server, 	type - 1 in case of client

}USER_INFO;


typedef struct{
	unsigned char		InBuf[ETH_RX_BUF_LEN];
	unsigned char		* RxPutPtr;
	unsigned char		* RxGetPtr;
//	unsigned char		OutBuf[ETH_TX_BUF_LEN];
//	unsigned char		* TxPutPtr;
//	unsigned char		* TxGetPtr;
}tcpport;


void tcpsocket_init(void);
//uint8_t packet_validates_for_active_clients(void);
void UCpacket_send_GUI(void);
void ping_cback (uint8_t event);
void udp_send_data(void);
void tcpsocket_init(void);
void udp_socket_init(void);
uint8_t ethernet_send(uint8_t socket, uint8_t *data_buf, uint16_t len);
void get_nw_settings(void);
uint8_t  set_nw_settings(struct json_struct *wms_payload_info, uint8_t num_bytes);
uint8_t send_data_json_server(uint8_t *data);
uint8_t connect_cloud_server(void);
#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


#ifndef __PORT_MAPPING_h__
#define	__PORT_MAPPING_h__

#define MAX_INPUT_PORTS						4
#define MAX_ONBOARD_OUTPUT_PORTS			4
										
#define INPUT_PORT_TYPE_DORMANT				0
#define	INPUT_PORT_TYPE_ON_OFF				1
#define	INPUT_PORT_TYPE_PIR					2
#define	INPUT_PORT_TYPE_FEEDBACK			3
#define INPUT_PORT_TYPE_MODE_SELECT			4

#define	OUTPUT_PORT_TYPE_DO					0
#define	OUTPUT_PORT_TYPE_AO					1
#define	OUTPUT_PORT_TYPE_DO_PULSE			2

#define PORT_STATE_ON						1
#define PORT_STATE_OFF						0
										
#define PORT_FOUND							1
#define PORT_NOT_FOUND						0

#define OCCUPANCY_NULL						0
#define OCCUPANCY_LOW						1
#define OCCUPANCY_MEDIUM					2
#define OCCUPANCY_HIGH						3

#define MINS(X)								(60*X)

struct __attribute__((__packed__)) port_param {			/* Structure for storing Device Info. */
	uint8_t send_pulse						:1;			/* Send Pulse Flag to be used in the case of DO_PULSE port type. */
	uint8_t pulse_state						:1;			/* Pulse state to be used in the case of DO_PULSE port type. */
	uint8_t output_port_type				:3;			/* Port Type. */
	uint8_t slave_add;									/* Output Device's Slave Address. */
	uint8_t port;										/* Digital Output Port Number. (64-127) or (152-183)*/
	uint8_t value;										/* Value (ON/OFF in case of DO & DO_PULSE, 0-100 in case of intensity) */
	struct port_param *next_port;						/* Pointer to next dev_info struct. */
};

struct __attribute__((__packed__)) ports_pool {			/* Structure for storing appropriate pointers to port param structures. */
	struct port_param *port_struct;
	struct ports_pool *next_port_struct;
};

struct __attribute__((__packed__)) input_port_mapping {
	uint8_t prev_state						:1;	
	uint8_t curr_state						:1;
	uint8_t valid_input_sensed				:1;
	uint8_t input_port_type					:4;
	uint8_t ports_active					:4;
	uint8_t input_debounce_counter			:4;
	uint8_t binding_key						:4;
	struct ports_pool *port_pool;
	struct pir_params *pir_info;
};

struct __attribute__((__packed__)) memory_port_info {
														/* Structure for storing Digital Output Info. */
	uint8_t slave_add;									/* Output Device's Slave Address. */
	uint8_t port;										/* Digital Output Port Number. (64-127) */
	uint8_t	output_port_type;							/* Port Type. */
};

struct __attribute__((__packed__)) pir_params {
	uint8_t sensor_count					:3;
	uint8_t activity_level					:3;
	uint8_t sense_window					:3;
	uint8_t null_window_counter				:6;
	uint16_t low_activity_config_time		:10;
	uint16_t medium_activity_config_time	:10;
	uint16_t high_activity_config_time		:10;
	uint8_t null_cnt_windows;							/* Upto 255 * 15 = 10 mins*/
	uint8_t decrease_percentage;						/* During NULL. */
	uint32_t on_duration;								/* 5, 10, 15 mins. */
};

void set_default (void);
void initialize_port_mapping (void);
void free_port_params (void);
void free_pir_params (void);
void free_attached_port_pools (void);

#endif

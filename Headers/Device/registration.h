#ifndef __REGISTRATION_H__
#define __REGISTRATION_H__
#include "stdint.h"
#include "packet.h"

#define		WAIT_FOR_TOKEN				1
#define		WAIT_FOR_REG_RESPONSE		2
#define 	REGISTRATION_DONE			3

uint32_t getLsd(uint32_t val);
void create_eco_wave_registration_pkt(void);
void ChkIfSlaveRegistered (void);
uint8_t validate_uid_rcvd (uint8_t *ptr);

#endif

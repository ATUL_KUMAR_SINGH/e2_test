/************************************************************************//**
* @file			bitops.h
*
* @brief		This file defines macros and declarations
*				for performing bit operations.
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @author		Mayank Mathur
*
* @note			Use Bit Access macros to Manipulate or Check a bit
*				in a Register or a Variable.
* @note			Use Bit-Addressable Variable declaration macro to declare a
*				bit addressable 8/16/32 bit variable.
****************************************************************************/
#ifndef BITOPS_H
#define BITOPS_H

/*
**===========================================================================
**		Include section
**===========================================================================
*/


/*
**===========================================================================
**						--- Bit access macros ---
**===========================================================================
*/
/************************************************************************//**
*@def			Bit(V)
*
* @brief		Mask bit V.
****************************************************************************/
#define		Bit(V)			(1 << (V))

/************************************************************************//**
*@def			Set_Bit(V,b)
*
* @brief		Sets the b(th) bit of V.
****************************************************************************/
#define 	Set_Bit(V,b)	(V = (V |(Bit(b))))

/************************************************************************//**
*@def			Clear_Bit(V,b)
*
* @brief		Clears the b(th) bit of V.
****************************************************************************/
#define		Clear_Bit(V,b)	(V = (V &(~(Bit(b)))))

/************************************************************************//**
*@def			Flip_Bit(V,b)
*
* @brief		Flips/Changes the b(th) bit of V.
****************************************************************************/
#define		Flip_Bit(V,b)	(V = (V ^(Bit(b))))

#define		BIT_SET	  	1

#define		BIT_CLEAR	 0

/************************************************************************//**
*@def			Is_Set(V,b)
*
* @brief		Returns TRUE if b(th) bit of V is Set else returns FALSE.
****************************************************************************/
#define		Is_Set(V,b)		((V & Bit(b)) ? BIT_SET : BIT_CLEAR)

/************************************************************************//**
*@def			Is_Clear(V,b)
*
* @brief		Returns TRUE if b(th) bit of V is Clear else returns FALSE.
****************************************************************************/
#define		Is_Clear(V,b)	((V & Bit(b)) ? BIT_CLEAR : BIT_SET)

/************************************************************************//**
*@def			Set_Bits(V,B)
*
* @brief		Sets those bits of V that are set in B.
****************************************************************************/
#define		Set_Bits(V,B)	(V = (V |(B)))

/************************************************************************//**
*@def			Clear_Bits(V,B)
*
* @brief		Clears those bits of V that are set in B.
****************************************************************************/
#define		Clear_Bits(V,B)	(V = (V &(~B)))

/************************************************************************//**
*@def			Flip_Bits(V,B)
*
* @brief		Flips those bits of V that are set in B.
****************************************************************************/
#define		Flip_Bits(V,B)	(V = (V ^(B)))

/************************************************************************//**
*@def			Are_Set(V,B)
*
* @brief		Returns TRUE if those bits of V are Set that are set in B else returns FALSE.
****************************************************************************/
#define		Are_Set(V,B)	((V & B) == B ? TRUE : FALSE)

/************************************************************************//**
*@def			Are_Clear(V,B)
*
* @brief		Returns TRUE if those bits of V are Clear that are set in B else returns FALSE.
****************************************************************************/
#define		Are_Clear(V,B)	((V & B) ? FALSE : TRUE)


/*
**===========================================================================
**					--- Bit-addressable Variables ---
**===========================================================================
*/
/************************************************************************//**
*@struct		__BITS8
*
* @brief		Structure for 8bit Bit-addressable Variable bits.
****************************************************************************/
typedef struct
{
  unsigned char no0:1;
  unsigned char no1:1;
  unsigned char no2:1;
  unsigned char no3:1;
  unsigned char no4:1;
  unsigned char no5:1;
  unsigned char no6:1;
  unsigned char no7:1;
} __BITS8;

/************************************************************************//**
*@struct		__BITS16
*
* @brief		Structure for 16bit Bit-addressable Variable bits.
****************************************************************************/
typedef struct
{
  unsigned short no0:1;
  unsigned short no1:1;
  unsigned short no2:1;
  unsigned short no3:1;
  unsigned short no4:1;
  unsigned short no5:1;
  unsigned short no6:1;
  unsigned short no7:1;
  unsigned short no8:1;
  unsigned short no9:1;
  unsigned short no10:1;
  unsigned short no11:1;
  unsigned short no12:1;
  unsigned short no13:1;
  unsigned short no14:1;
  unsigned short no15:1;
} __BITS16;

/************************************************************************//**
*@struct		__BITS32
*
* @brief		Structure for 32bit Bit-addressable Variable bits.
****************************************************************************/
typedef struct
{
  unsigned long no0:1;
  unsigned long no1:1;
  unsigned long no2:1;
  unsigned long no3:1;
  unsigned long no4:1;
  unsigned long no5:1;
  unsigned long no6:1;
  unsigned long no7:1;
  unsigned long no8:1;
  unsigned long no9:1;
  unsigned long no10:1;
  unsigned long no11:1;
  unsigned long no12:1;
  unsigned long no13:1;
  unsigned long no14:1;
  unsigned long no15:1;
  unsigned long no16:1;
  unsigned long no17:1;
  unsigned long no18:1;
  unsigned long no19:1;
  unsigned long no20:1;
  unsigned long no21:1;
  unsigned long no22:1;
  unsigned long no23:1;
  unsigned long no24:1;
  unsigned long no25:1;
  unsigned long no26:1;
  unsigned long no27:1;
  unsigned long no28:1;
  unsigned long no29:1;
  unsigned long no30:1;
  unsigned long no31:1;
} __BITS32;

/************************************************************************//**
*@def			Bit8(NAME)
*
* @brief		Define NAME as a 8bit addressable variable. \n
* @detail		Access of 8bit var:		NAME.val \n
*				Access of bit(s):		NAME.bit.noX  (X=0-7)
****************************************************************************/
#define Bit8(NAME)\
		union{\
			unsigned char val;\
			__BITS8 bit;\
		}NAME;

/************************************************************************//**
*@def			Bit16(NAME)
*
* @brief		Define NAME as a 16bit addressable variable. \n
* @detail		Access of 16bit var:		NAME.val \n
*				Access of bit(s):		NAME.bit.noX  (X=0-15)
****************************************************************************/
#define Bit16(NAME)\
		union{\
			unsigned short int val;\
			__BITS16 bit;\
		}NAME;

/************************************************************************//**
*@def			Bit32(NAME)
*
* @brief		Define NAME as a 32bit addressable variable. \n
* @detail		Access of 32bit var:		NAME.val \n
*				Access of bit(s):		NAME.bit.noX  (X=0-31)
****************************************************************************/
#define Bit32(NAME)\
		union{\
			unsigned long int val;\
			__BITS32 bit;\
		}NAME;

#endif // BITOPS_H

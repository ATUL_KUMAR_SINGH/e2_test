/************************************************************************************************************************************************************
* @file			BUZZER.h
*
* @brief		Header for BUZZER.c.
*
* @attention	Copyright 2013 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @author		Sahil Saini
*
************************************************************************************************************************************************************/

#ifndef BUZZER_H_
#define BUZZER_H_

/************************************************************************************************************************************************************
 * Include Section
 ***********************************************************************************************************************************************************/
#include <stdint.h>
#include "water_management.h"
#include"json_client.h"

/************************************************************************************************************************************************************
 * Defines section
************************************************************************************************************************************************************/
#define BUZZER_ON		1
#define BUZZER_OFF		0
#define	SNOOZE_STATE	2

//#define BUZZER_OFF								(uint16_t)0
#define BUZZER_5_MIN							(uint16_t)300
#define BUZZER_1_MIN							(uint16_t)60

/************************************************************************************************************************************************************
 * Global Variable Declaration Section
 ***********************************************************************************************************************************************************/
//typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
//	uint8_t 	blow_buzzer_flag 			:1;				// stored in ram.
//	uint8_t 	snooze_buzzer_flag			:1;				// stored in ram.
//	uint8_t 	buzzer_state				:1;				// stored in ram.
//	uint8_t		buzzer_switch_state			:1;				// stored in ram.
//	uint8_t		buzzer_switch_pressed_cntr;					// stored in ram.
//	uint8_t 	buzzer_active_flag;							// stored in eeprom.
//	uint8_t 	buzzer_snooze_interval;						// stored in ram.
//	uint8_t		buzzer_kill_flag;							// stored in ram.
//	uint8_t		buzzer_port;								// stored in ram.
//	uint16_t	buzzer_interval;							// stored in ram.
//}buzz;
//extern buzz buzzer;

struct buzzer_interval{
	uint8_t beep_time;
	uint8_t	interval;
	uint16_t snooze_time;
	uint8_t		buzzer_snooze_flag;

};

struct buzzer_switch_behavior{
	uint8_t		buzzer_activate_flag;
	uint8_t		buzzer_state;
	uint8_t		action_num;
	uint8_t		total_duration;
	uint8_t		buzzer_on_period;			// in seconds
	uint8_t		buzzer_off_period;
	uint16_t	buzzer_snooze_dur;
	
};

/***********************************************************************************************************************************************************
 * Prototype Section
 ***********************************************************************************************************************************************************/
void blow_buzzer(uint8_t state);
void beep_once(void);
void seconds_based_buzzer_op(void);

#define	BUZZER_SWITCH_PORT	  	 GPIOC

#define BUZZER_SWTCH            GPIO_Pin_8

#define	READ_RESET_BUZZER_SWITCH		GPIO_ReadInputDataBit(BUZZER_SWITCH_PORT, BUZZER_SWTCH)

#define BUZZER_SWITCH_CLOCK_PORT				RCC_AHB1Periph_GPIOC
#define BUZZER_SWITCH_EXTI_PortSource			EXTI_PortSourceGPIOC
#define BUZZER_SWITCH_EXTI_PinSource	    	EXTI_PinSource8
#define BUZZER_SWITCH_EXTI_Line					EXTI_Line8
#define BUZZER_SWITCH_EXTI_IRQn					EXTI4_15_IRQn


#define	BUZZER_CLOCK_PORT		RCC_AHBPeriph_GPIOB

#define	BUZZER_PORT						GPIOB

#define	BUZZER						GPIO_Pin_4


#define	SET_BUZZER	  	GPIO_SetBits(BUZZER_PORT, BUZZER) 

#define	CLR_BUZZER	  	GPIO_ResetBits(BUZZER_PORT, BUZZER) 

#define OHT_MOTOR_RELAY				GPIO_Pin_10
#define MOTOR_PORT						GPIOB
#define MOTOR_CLOCK_PORT			RCC_AHBPeriph_GPIOB

#define UGT_MOTOR_RELAY				GPIO_Pin_2
#define MOTOR_PORT						GPIOB
#define MOTOR_CLOCK_PORT			RCC_AHBPeriph_GPIOB

#define	SET_OHT_MOTOR	  	GPIO_SetBits(MOTOR_PORT, OHT_MOTOR_RELAY) 

#define	CLR_OHT_MOTOR	  	GPIO_ResetBits(MOTOR_PORT, OHT_MOTOR_RELAY) 

#define	SET_UGT_MOTOR	  	GPIO_SetBits(MOTOR_PORT, UGT_MOTOR_RELAY) 

#define	CLR_UGT_MOTOR	  	GPIO_ResetBits(MOTOR_PORT, UGT_MOTOR_RELAY) 






#define OFF   0

#define ON   1

#define OHT_PUMP 	0x80

#define UGT_PUMP 	0x0

#define	OHT_MOTOR_ON_CASE	0x81

#define	OHT_MOTOR_OFF_CASE	0x80

#define	UGT_MOTOR_ON_CASE	0x01

#define	UGT_MOTOR_OFF_CASE	0x0

#define	PUMP_STATUS_CHANGE		0x00000001

#define	BUZZER_ID				51


void motor_buzzer_relay_init(void);
void trigger_motor_switch(uint8_t tank_type, uint8_t motor_flag);
void motor_buzzer_switch_init(void);
void buzzer_behavior_select(void);
uint8_t actuate_motor(uint8_t tank_type, uint8_t state);
void update_dry_run_counter(void);
void buzzer_blow(void);
uint32_t pump_trigger_save_status(uint8_t tank_num);
void get_buzzer_settings(void);
void get_tank_pump_settings(void);
void save_buzzer_setting(struct json_struct *wms_payload_info, uint8_t num_bytes);
void get_pump_status(struct tank_info *tank_ptr);
void get_tank_pump_status(void);
#endif /* BUZZER_H_ */

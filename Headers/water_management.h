/************************************************************************//**
* @file			water_management.h
*
* @brief		Header for water_management.c
*
* @attention	Copyright 2014 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 27/012/11 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/

#ifndef WATER_MANAGEMENT_H
#define WATER_MANAGEMENT_H


#include"json_client.h"
#include <stm32f0xx.h>
/*
**===========================================================================
**		Modules define section
**===========================================================================
*/

//#define	 VIRTUAL_SENSOR					1

#define		OHT				1
			
#define		UGT				2

#define	 UGT_PUMP_ENABLE				1

#define	 DRY_RUN_ENABLE					1

#define	 SCHEDULE_ENABLE				1	 

#define	 CONSUMPTION_LOG_ENABLE			1

//#define	 ANALOG_SENSOR_ENABLE		1

#define	 LEVEL_CHANGE_RECORD_ENABLE		1

//#define	 PUMP_RECORD_ENABLE				1

#define	 INFLOW_CALCULATION_ENABLE		1

#define	 BUZZER_BEHAVIOUR_ENABLE		1

#define	 FEEDBACK_ENABLE				1

#define	PRESENCE_SENSOR_ENABLE			1

#define	CLOUD_SERVICE_ENABLE			1

//#define	EVENT_ENABLE					1

//#define	NOTIFICATION_ENABLE				1

#define	MULTI_TANK_ENABLE				1


/*
**===========================================================================
**		memory address layout define
**===========================================================================
*/

//#define	TOTAL_TANKS						9



//#define	 TOTAL_VIRTUAL_SENSOR				11	

//#define	UGT_PUMP_STATUS						0
//
//#define	SCHEDULE_RUN_ERROR					1
//
//#define PUMP_WIRE_DISCONNECTED_ERROR		7
//
//#define PUMP_TRIGGER						31
//
//#define OHT_LEVEL_CHANGE					30
//
//#define UGT_LEVEL_CHANGE					29

#define	PUMP_TRIGGER					0

/**** Notification macros**********************/
#define	SENSOR_MALFUNCTIONING			1

#define	SENSOR_HIT_6_TIMES_MIN			2

#define	PUMP_TRIGGR_4_TIMES_MIN			3

#define	PUMP_WIRE_DISCONNECT			4

#define	PUMP_WIRE_CONNECT				5

#define	PUMP_CANT_START_WIRE_DISCONNECT		6

#define	PUMP_CANT_START_AC_MAINS_OFF		7   

#define	AUTOMATED_TASK_1				8 

#define	AUTOMATED_TASK_2				9 

#define	AUTOMATED_TASK_3				10 

#define	AUTOMATED_TASK_4				11 

#define	AUTOMATED_TASK_5				12 

#define	AUTOMATED_TASK_6				13 

#define	AUTOMATED_TASK_7				14 

#define	AUTOMATED_TASK_8				15 

#define	AUTOMATED_TASK_9				16

#define	SCHEDULE_CANT_RUN				17

#define	SYSTEM_ON_BATTERY				18

#define	SYSTEM_ON_MAINS					19

#define	NEW_FIRMWARE_UPGRADE_SUCCESSFULLY		20

#define	NEW_FIRMWARE_UPGRADE_FAILURE			21

#define	DISCONNECT_FROM_SERVER					22

#define	CONNECTION_ESTABLISH_SERVER				23

#define	PRIMARY_IMAGE_RUNNING					24

#define	SECONDARY_IMAGE_RUNNING					25

//////////////////////////////////////////////////////////
/****** events macro **********************************/

#define	OHT_PUMP_TRIGGER					1

#define	UGT_PUMP_TRIGGER					8

#define	OHT_SENSOR_TRIGGER					16

#define	UGT_SENSOR_TRIGGER					24


#define	TOTAL_NOTIFICATION						20

#define SENSOR_READ_TIME						15


#define	UDP_ALWAYS_ON							1

#define	POWER_ON								1

#define	RESET_SWITCH							2

#define	SOFT_SWITCH								3

#define	CHANGE_IP								4


/*
**===========================================================================
**		MACROS define
**===========================================================================
*/


#define	SENSOR_MONITORING_10_SEC			1

#define	MANUAL_OHT_PUMP_TRIGGER				2

#define	MANUAL_UGT_PUMP_TRIGGER				4

#define	FORCEFULLY_OHT_PUMP_TRIGGER			8

#define	FORCEFULLY_UGT_PUMP_TRIGGER			16

#define	SCHEDULE_START_OHT_TRIGGER			32

//#define	SCHEDULE_END_OHT_TRIGGER			33

#define	SCHEDULE_START_UGT_TRIGGER			64

//#define	SCHEDULE_END_UGT_TRIGGER			65

#define	SEND_HOURLY_CONSUMPTION				128

#define	DRY_RUN_OHT_TRIGGER					256

#define	DRY_RUN_UGT_TRIGGER					512

#define	POWER_FAILURE_PUMP_OFF				1024

#define	AUTOMATIC_OHT_TRIGGER				2048

#define	AUTOMATIC_UGT_TRIGGER				4096



#define	EVT_MANUAL_OHT_PUMP_TRIGGER				1

#define	EVT_MANUAL_UGT_PUMP_TRIGGER				2

#define	EVT_FORCEFULLY_OHT_PUMP_TRIGGER			3

#define	EVT_FORCEFULLY_UGT_PUMP_TRIGGER			4

#define	EVT_SCHEDULE_START_OHT_TRIGGER			5

//#define	EVT_SCHEDULE_END_OHT_TRIGGER			6

#define	EVT_SCHEDULE_START_UGT_TRIGGER			6

//#define	EVT_SCHEDULE_END_UGT_TRIGGER			8

#define	EVT_SEND_HOURLY_CONSUMPTION				7

#define	EVT_DRY_RUN_OHT_TRIGGER					8

#define	EVT_DRY_RUN_UGT_TRIGGER					9

#define	EVT_POWER_FAILURE_PUMP_OFF				10

#define	EVT_AUTOMATIC_OHT_TRIGGER				11

#define	EVT_AUTOMATIC_UGT_TRIGGER				12






#define	MOTOR_TRIGGER_FROM_SWITCH				1

#define	MOTOR_TRIGGER_FROM_GUI					2

#define	MOTOR_TRIGGER_FROM_SCHEDULE				3

#define	MOTOR_TRIGGER_FROM_DRY_RUN				4

#define	MOTOR_TRIGGER_FROM_AUTO_TASK			5

#define MOTOR_TRIGGER_POWER_FAILURE				6

#define CUSTOM_TYPE					0

#define CUBICAL_TYPE				1

#define CYLINDRICAL_TYPE			2

//#define	ADMIN_PASSWRD_LEN			 6

#define	SERVER_CONNECTIVITY_IDLE				3

#define	SERVER_CONNECTIVITY_DISCONNECT			2

#define	SERVER_CONNECTIVITY_CONNECT				1

//#define	SUMBERSIBLE_PUMP						1

#define	SYSTEM_TRIGGER						 	1

#define	SUMBERSIBLE_TRIGGER					 	2





/*
**===========================================================================
**		STRUCTURES define
**===========================================================================
*/

#ifdef MULTI_TANK_ENABLE
   	struct   wms_sys_info{
		uint8_t			total_oht;
		uint8_t			total_ugt;
	//	uint8_t			oht_pump_select;
		uint8_t			ugt_pump_select;
	
	};


#pragma pack(1)
struct  pump_info{
	uint8_t 	pump_state;
	uint8_t 	last_on_off_time[6];
	uint8_t 	switch_trigger_flag;
	uint8_t		trigger_cause;		/* from switch	-	01,  from GUI	-	02, from schedule - 03, from dry run - 04,   from automated task - 05        */
};

	struct tank_status{
		uint8_t  oht_current_level;
		uint8_t  ugt_current_level;
		struct	 pump_info *oht_pump_ptr;
		struct	 pump_info *ugt_pump_ptr;
	};

#define TOTAL_OHT_TANK		2



#else

	struct   wms_sys_info{
		uint8_t 		total_tank;
		uint8_t 		total_pump;
//		uint8_t			oht_pump_select;
//		uint8_t			ugt_pump_select;
	};
	
	struct   backup_info{
		uint8_t 	tank_dimension; 		//
	//	uint8_t		OHT_equidistant_flag;
	//	uint8_t		UGT_equidistant_flag;
		
	};

//#define TOTAL_OHT_TANK		1

	

#endif
	#pragma pack(1)
	struct   real_sensor_info{
		uint8_t 	total_sensor;
		uint8_t		equidistant_flag;
		uint8_t		highest_set_sensor;
		uint8_t 	port_num[16];
		uint8_t		cap_bw_sensor_perc[16];
		uint16_t  	sensor_status_bits;	
		uint16_t  	prev_sensor_status_bits;	
		uint16_t  	malfunctioning_sensor_bits;
		uint16_t  	prev_malfunctioning_sensor_bits;
		
		
	
	}; 
	
	struct virtual_sensor_info{
		uint8_t		total_sensor;
		uint8_t		cap_bw_sensor_perc[21];
		uint16_t  	sensor_status_bits;	
		uint16_t  	malfunctioning_sensor_bits;	
		uint8_t		highest_set_sensor;
	
	}; 
	
	#pragma pack(1)
	struct tank_setting{
		uint8_t     tank_num;			/* for OHT Tank num 1 - 25,  UGT Tank num 26 - 50. */
		uint8_t     tank_state;			/* Custom type - 0, cubical - 1, cylindrical - 2  */
		uint8_t		tank_name[20];	
		uint32_t 	tank_volume;
	};
	#pragma pack(1)
	struct   tank_info{
		struct		tank_setting *tank_config_ptr;
		uint8_t		pump_select;
		uint8_t		flow_rate;
		uint32_t 	current_volume;
		uint8_t		current_level;
		uint8_t		prev_level;
		uint8_t		next_hit_level;
		uint8_t		virtual_current_level;
		uint32_t	current_day_consumption;	
		struct 		real_sensor_info *real_sensor_ptr;
		struct		virtual_sensor_info	*virtual_sensor_ptr;

	};



#pragma pack(1)
struct auto_task{
	uint8_t 	tsk_enable_flag[9];		  /* 7 configurable + 2 inherent task */
	uint8_t		tsk_1_preset_level;
	uint8_t  	tsk_2_preset_level;
//	uint8_t  	tsk_3_preset_level;
	uint8_t  	tsk_4_preset_level;
	uint8_t  	tsk_5_preset_level;
	uint8_t  	tsk_6_preset_level[2];
	uint8_t  	tsk_7_preset_level[4];
	uint8_t		tsk_9_preset_level;
};
	
	 
	
	
struct loc_tank_trigger_flags{
	uint8_t		oht_level_change_flag;
	uint8_t		oht_sensor_malfnctng_flag;
	uint8_t		oht_motor_trigger_flag;
	uint8_t		ugt_level_change_flag;
	uint8_t		ugt_sensor_malfnctng_flag;
	uint8_t		ugt_motor_trigger_flag;
	uint8_t		prev_ugt_highest_set_sensor;
	uint8_t		prev_oht_highest_set_sensor[TOTAL_OHT_TANK];
	uint8_t		pump_cause;
};

struct tank_immediate_setting{
	uint8_t pump_state;
	uint8_t year;
	uint8_t	month;
	uint8_t	date;
	uint8_t	hour;
   	uint8_t	min;
	uint8_t	level;
};

void restore_configured_settings(void);
void initialize_global_structures(void);
void write_default_configuration(void);
void clear_consumption_logs(void);
void write_def_tank_pump_settings(void);


#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
